//
//  EventManager.swift
//  Utilities
//
//  Created by Filip Duvnjak on 12/17/18.
//  Copyright © 2018 Near Lock. All rights reserved.
//

import Foundation
import UIKit

class ContactsViewController: AdvancedListViewController {
    var contacts = getAllContactNames()
    
    static func initViewController() -> ContactsViewController {
        return ContactsViewController(nibName: "FullscreenListViewController", bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cells = self.createItemVM()
    }
    
    func createItemVM() -> [Item] {
        var items:[Item] = []
        items.append((ItemDetail3().setTitle(title: "test").setDetail(detail: "detail Text")).onAction {
            self.navigationController?.pushViewController(AdvancedListDemo.createDemoViewController(), animated: true)
        })
        items.append(contentsOf: self.cellItemsForContacts(contacts: self.contacts))
        return items
    }
    
    func cellItemsForContacts(contacts:[String]) -> [Item] {
        var items:[Item] = []

        for contact in self.contacts {
            items.append((ItemDetail3().setTitle(title: contact).setDetail(detail: "detail Text")).onAction {
                    self.showContact(contactName: contact)
                }.onDelete {
                    self.deleteContact(contactName: contact)
            })
        }
        return items
    }
    
    func deleteContact(contactName:String) {
        if let index = self.contacts.index(of:contactName) {
            self.contacts.remove(at: index)
            self.cells = self.createItemVM()
            self.reloadData()
        }
    }
    
    func showContact(contactName:String) {
        let detailVC = ContactsDetailViewController.initFromStoryBoard()
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    static func getAllContactNames() -> [String] {
        return ["Filip", "Ante", "Igor", "Mislav", "Tomislav"]
    }
    
}
