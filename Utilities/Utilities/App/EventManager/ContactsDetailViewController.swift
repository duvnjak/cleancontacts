//
//  ContactsDetailViewController.swift
//  Utilities
//
//  Created by Filip Duvnjak on 12/17/18.
//  Copyright © 2018 Near Lock. All rights reserved.
//

import Foundation
import UIKit

class ContactsDetailViewController : UIViewController {
    static func initFromStoryBoard() -> ContactsDetailViewController {
        return ContactsDetailViewController.fromStoryboard(named: "Contacts")
    }
}
