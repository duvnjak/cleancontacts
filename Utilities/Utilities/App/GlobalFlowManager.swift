//
//  GlobalFlowManager.swift
//  Utilities
//
//  Created by Filip Duvnjak on 12/16/18.
//  Copyright © 2018 Near Lock. All rights reserved.
//

import Foundation
import UIKit

class GlobalFlowManager {
    
    var rootNavController:UINavigationController? = nil
    var rootSideViewController:UIViewController? = nil
    var rootViewController:UIViewController? = nil
    
    var flowResources:FlowResources?
    
    public func instantiateRootViewController() -> UIViewController {
        self.flowResources = FlowResources()
        
//        self.rootViewController = AdvancedListDemo.createDemoViewController()
        self.rootViewController = ContactsViewController.initViewController()

        self.rootNavController = AviraVCFactory().createAppRootNavController(rootViewController: self.rootViewController!)
        return self.rootNavController!
    }
    
    public func startAppFlowLogic(){

    }
    
}
