//
//  AviraVCFactory.swift
//  Avira Cleaner
//
//  Created by Filip Duvnjak on 11/6/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit
//import SlideMenuControllerSwift

class AviraVCFactory {
//    func createSideMenu(with mainVC:UIViewController, leftVC:UIViewController) -> UIViewController {
//        if (self.useCollapsableSplitViewControllerForIpad() && AVMacros().IS_IPAD()) {
//            let splitVC = UISplitViewController()
//            splitVC.viewControllers = [leftVC, mainVC]
//            return splitVC
//        }else{
//            SlideMenuOptions.contentViewScale = 1.0
//            SlideMenuOptions.tapGesturesEnabled = true
//            SlideMenuOptions.panGesturesEnabled = true
//            SlideMenuOptions.hideStatusBar = false
//            SlideMenuOptions.shadowOpacity = 0.75
//            SlideMenuOptions.shadowOffset = CGSize(width: 0, height: -1)
//            SlideMenuOptions.shadowRadius = 4.0
//            SlideMenuOptions.leftViewWidth = AVMacros().IS_IPAD() ? 270 : UIScreen.main.bounds.width*0.8
//            return SlideMenuController(mainViewController: mainVC, leftMenuViewController: leftVC)
//        }
//    }
    
//    func useCollapsableSplitViewControllerForIpad() -> Bool {
//        //TODO: setup different libraries for the iPad and the iPhone - like the Avira VPN app
//        return false
//    }
    
//    func createAboutViewController(backAction:@escaping (()->())) -> AboutViewController {
//        let vc:AboutViewController = AboutViewController.fromStoryboard(named: "AboutViewController")
//        vc.backAction = {
//            backAction()
//        }
//        return vc
//    }
    
    func createAppRootNavController(rootViewController:UIViewController) -> UINavigationController {
        let navController = UINavigationController(rootViewController:rootViewController)
        navController.setNavigationBarHidden(false, animated: false)
        navController.navigationBar.isTranslucent = false
        navController.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        navController.navigationBar.shadowImage = UIImage()
        return navController
    }
    
    func navWrapper(viewController:UIViewController) -> UINavigationController {
        let navController = UINavigationController(rootViewController:viewController)
        navController.setNavigationBarHidden(true, animated: false)
        return navController
    }
    
//    func showTermsAndAgreements(on viewController:UIViewController, firstTimeUser:Bool, completion:(()->())? = nil) {
//        let termsVC = FullscreenAlertVC.initFromStoryBoard()
//        termsVC.onViewDidLoad = {
//            termsVC.setupTermsVC(firstTimeUser: firstTimeUser)
//        }
//        termsVC.mainButtonAction = {
//            viewController.dismiss(animated: true, completion: {
//                completion?()
//            })
//        }
//        viewController.present(termsVC, animated: true)
//    }
    
//    func showFTU(_ animated:Bool, on viewController:UIViewController, completion:(()->())? = nil) {
//        let ftuVC = AviraOptimizerOnboarding.initFromNib()
//        ftuVC.setupFinishedAction = {
//            AVUser.sharedInstance().userCompletedTheSignupProcess()
//            viewController.dismiss(animated: animated, completion: {
//                completion?()
//            })
//        }
//        viewController.present(ftuVC, animated: animated)
//    }
}
