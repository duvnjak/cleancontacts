//
//  BaseCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/4/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class Item {
//    func cellType() -> BaseCell.Type {return BaseCell.self}
//    func newCell() -> BaseCell {return cellType().init(item:self)}

    func newCell() -> BaseCell {return BaseCell(item: self)}
    func reuseIdentifier() -> String {return "reuse_identifier"}
    func nibName() -> String {return self.reuseIdentifier()}

    var liveCellUpdateHandler: ((_ animated: Bool)->())?
    
    var fontSize = 0;
    var fontBold = false
    var backgroundColor:UIColor? = nil
    var tintColor:UIColor? = nil
    var rowId: String? = nil
    var title: String?
    var detail: String?
    var detail2: String?
    var imageName: String? = nil
    var imageName2: String? = nil
    var userInfo: Any? = []
    var forcedHeight:CGFloat = -1
    var disableCellSelection = false
    var hideSeparator = false
    var showCustomSeparator = false
    var attributedTitle:NSAttributedString? = nil
    var textAlignment:NSTextAlignment? = nil
    
    //Actions
    var action: (()->())?
    var deleteAction: (()->())? = nil
    var didDisplayAction: (()->())? = nil
    var didHideAction: (()->())? = nil
    
    public init(){
        
    }
    
    convenience init(rowId:String? = nil,
                     title: String = "",
                     detail: String = "",
                     imageName: String? = nil,
                     userInfo: Any? = nil,
                     customSeparator:Bool = false,
                     hideSeparator:Bool = false,
                     height:CGFloat? = -1) {
        self.init()
        self.rowId = rowId
        self.title = title
        self.detail = detail
        self.imageName = imageName
        self.showCustomSeparator = customSeparator
        self.hideSeparator = hideSeparator
        if height != -1 {
            self.forcedHeight = height!
        }
    }
    
    func setAttributedTitle(title:NSAttributedString) -> Self {
        self.attributedTitle = title
        return self
    }
    
    func setBackgroundColor(color:UIColor) -> Self {
        self.backgroundColor = color
        return self
    }
    
    func setTintColor(color:UIColor) -> Self {
        self.tintColor = color
        return self
    }
    
    func setTitle(title:String, animated:Bool = false) -> Self  {
        self.title = title
        self.liveCellUpdateHandler?(animated)
        return self
    }
    
    func setImageName(imageName:String, animated:Bool = false) -> Self  {
        self.imageName = imageName
        self.liveCellUpdateHandler?(animated)
        return self
    }
    
    func setDetail(detail:String, animated:Bool = false) -> Self  {
        self.detail = detail
        self.liveCellUpdateHandler?(animated)
        return self
    }
    
    func setForcedHeight(height:CGFloat) -> Self {
        self.forcedHeight = height
        return self
    }
    
    func onDelete(delete:@escaping (()->(Void))) -> Self {
        self.deleteAction = delete
        return self
    }
    
    func onAction(action:@escaping (()->(Void))) -> Self {
        self.action = action
        return self
    }
    
    func onDidDisplay(action:@escaping (()->(Void))) -> Self {
        self.didDisplayAction = action
        return self
    }
    
    func onDidHide(action:@escaping (()->(Void))) -> Self {
        self.didHideAction = action
        return self
    }
}

class BaseCell: UITableViewCell {    
    var cellItem:Item?
    var didLoad = false
    
    class func reuseIdentifier() -> String {
        return "UITableViewCell"
    }
    
    class func nibName() -> String {
        return self.reuseIdentifier()
    }
    
    init(style: UITableViewCellStyle = .default, item:Item? = nil) {
        super.init(style: style, reuseIdentifier: item?.reuseIdentifier())
        self.cellItem = item
    }
    
     convenience init(item:Item) {
        self.init(style: .default, reuseIdentifier: item.reuseIdentifier())
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func updateCell(forCellItem cellItem:Item) {
        self.updateCell(forCellItem: cellItem, animated:false)
    }
    
    public func cellDidLoad(){
        //override if needed
    }
    
    public func updateCell(forCellItem cellItem:Item, animated:Bool = false) {
        self.cellItem = cellItem
        
        if !self.didLoad {
            self.didLoad = true
            self.cellDidLoad()
        }
        
        if (cellItem.action != nil && cellItem.disableCellSelection != true) {
            self.selectionStyle = .default
        }else{
            self.selectionStyle = .none
        }
        
        if let backgroundColor = cellItem.backgroundColor {
            self.backgroundColor = backgroundColor
            self.contentView.backgroundColor = backgroundColor
        }else{
            self.backgroundColor = UIColor.white
            self.contentView.backgroundColor = UIColor.white
        }
        
        self.cellItem?.liveCellUpdateHandler = { animated in
            self.updateCell(forCellItem: self.cellItem!, animated:animated)
        }
        
        if (self.cellItem?.hideSeparator)! {
            self.hideSeparator()
        }else if (self.cellItem?.showCustomSeparator)! {
            self.showEndToEndSeparator()
        }else{
            self.showDefaultSeparator()
        }
    }
    
}
