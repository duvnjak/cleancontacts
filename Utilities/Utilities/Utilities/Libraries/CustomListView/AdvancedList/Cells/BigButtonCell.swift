//
//  BigButtonCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 6/7/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemBigButton : Item {
    override func newCell() -> BaseCell {return BigButtonCell(item: self)}
    override func reuseIdentifier() -> String {return "BigButtonCell"}
}

class BigButtonCell: BaseCell {
    override class func reuseIdentifier() -> String { return "BigButtonCell" }

    func castItem() -> ItemBigButton { return cellItem as! ItemBigButton}
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var customImageView: UIImageView!
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        self.label.text = castItem().title
        self.label.textColor = App.AppColor.FlowerKid.grayIcon
        self.customImageView.image = UIImage(named: castItem().imageName ?? "") ?? nil
    }
    
    @IBAction func didToggleButton(_ sender: AnyObject) {
        self.cellItem?.action?()
    }
}


