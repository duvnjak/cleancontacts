//
//  SmallButtonCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 6/19/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemButtonSmall : Item {
    override func newCell() -> BaseCell {return SmallButtonCell(item: self)}
    override func reuseIdentifier() -> String {return "SmallButtonCell"}
}

class SmallButtonCell: BaseCell {
    override class func reuseIdentifier() -> String { return "SmallButtonCell" }

    func castItem() -> ItemButtonSmall { return cellItem as! ItemButtonSmall}
    @IBOutlet weak var button: UIButton!
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        self.button?.setTitle(castItem().title, for: .normal)
        
        if let tint = castItem().tintColor {
            self.button?.backgroundColor = tint
        }
    }
    
    @IBAction func didToggleButton(_ sender: AnyObject) {
        self.cellItem?.action?()
    }
}
