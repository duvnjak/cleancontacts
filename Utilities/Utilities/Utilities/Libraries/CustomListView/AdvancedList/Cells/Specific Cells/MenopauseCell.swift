//
//  MenopauseCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/23/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemMenopause : Item {
    override func reuseIdentifier() -> String {return "MenopauseCell"}
    override func newCell() -> BaseCell {return MenopauseCell(style: .default, reuseIdentifier: self.reuseIdentifier())}
    
    var estimate:MenopauseEstimate?
    
    convenience init(estimate:MenopauseEstimate) {
        self.init()
        self.estimate = estimate
        self.forcedHeight = 100
    }
}

enum MenopauseEstimate:String {
    case beforeNormal = "before normal"
    case normal = "normal"
    case afterNormal = "after normal"
}

class MenopauseCell: BaseCell {
    func castItem() -> ItemMenopause { return cellItem as! ItemMenopause}
    @IBOutlet weak var view1: CircleLabelView!
    @IBOutlet weak var view2: CircleLabelView!
    @IBOutlet weak var view3: CircleLabelView!
    @IBOutlet weak var view4: CircleLabelView!
    @IBOutlet weak var view5: CircleLabelView!

    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
    
        view1.label.text = "48"
        view1.setTintColor(color: UIColor.lightGray)
        
        view2.label.text = "49"
        view2.setTintColor(color: UIColor.lightGray)

        view3.label.text = "50"
        view3.setTintColor(color: UIColor.lightGray)

        view4.label.text = "51"
        view4.setTintColor(color: UIColor.lightGray)

        view5.label.text = "52"
        view5.setTintColor(color: UIColor.lightGray)
        
        let estimate = castItem().estimate
    
        if estimate == .beforeNormal {
            view1.label.text = "..."
            view1.setTintColor(color: App.AppColor.FlowerKid.mainColor)
            view2.setTintColor(color: App.AppColor.FlowerKid.mainColor)
            view3.setTintColor(color: App.AppColor.FlowerKid.mainColor)
        }else if estimate == .normal {
            view3.setTintColor(color: App.AppColor.FlowerKid.mainColor)
        }else if estimate == .afterNormal {
            view5.label.text = "..."
            view3.setTintColor(color: App.AppColor.FlowerKid.mainColor)
            view4.setTintColor(color: App.AppColor.FlowerKid.mainColor)
            view5.setTintColor(color: App.AppColor.FlowerKid.mainColor)
        }
    }
}
