//
//  ReviewPurchaseCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 6/7/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class LabReviewPurchase : ItemReviewPurchase {
    convenience init(stars:Double) {
        self.init()
        self.forcedHeight = 260
        self.imageName = "lab-real-large"
        self.title = "home-lab".localized
        self.detail = "home-lab-details".localized
        self.detail2 = "199.00€"
        self.backgroundColor = App.AppColor.FlowerKid.mainColor
    }
}


public class ItemReviewPurchase : Item {
    override func reuseIdentifier() -> String {return "ReviewPurchaseCell"}
    override func newCell() -> BaseCell {return ReviewPurchaseCell(style: .default, reuseIdentifier: self.reuseIdentifier())}
}

class ReviewPurchaseCell: BaseCell {
    func castItem() -> ItemReviewPurchase { return cellItem as! ItemReviewPurchase}
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var customImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var ratingView: UIView!
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        self.titleLabel.text = castItem().title
        self.descriptionLabel.text = castItem().detail
        self.priceLabel.text = castItem().detail2
        
        self.customImageView.image = UIImage(named: castItem().imageName ?? "") ?? nil
    }
    
    @IBAction func didToggleButton(_ sender: AnyObject) {
        self.cellItem?.action?()
    }
}

