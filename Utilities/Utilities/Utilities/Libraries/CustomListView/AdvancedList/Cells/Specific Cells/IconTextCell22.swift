//
//  ItemIconText.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 4/30/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemIconText22 : Item {
    override func newCell() -> BaseCell {return IconTextCell22(style: .default, reuseIdentifier: self.reuseIdentifier())}
    override func reuseIdentifier() -> String {return "IconTextCell22"}
}

class IconTextCell22: BaseCell {
    @IBOutlet weak var boarderView: UIView?
    @IBOutlet weak var icon: UIImageView?
    @IBOutlet weak var detailImageView: UIImageView?
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    
    func castItem() -> ItemIconText22 { return cellItem as! ItemIconText22}

    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.setupCellLayout()
        
        self.titleLabel?.text = castItem().title
        self.descriptionLabel?.text = castItem().detail
        self.icon?.image = UIImage(named: cellItem.imageName ?? "") ?? nil
        if let tintColor = cellItem.tintColor{
            self.icon?.setImageColor(color: tintColor)
        }
    }
    
    @IBAction func didToggleButton(_ sender: AnyObject) {
        self.cellItem?.action?()
    }
    
    func setupCellLayout() {
        self.titleLabel?.textColor = App.AppColor.FlowerKid.mainColor
        self.descriptionLabel?.textColor = App.AppColor.FlowerKid.textBlack
        
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        self.boarderView?.styleBoarderedView()
        self.boarderView?.backgroundColor = UIColor.white
    }
}
