//
//  IconTextCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 8/21/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemIconTextCell : Item {
    override func newCell() -> BaseCell {return IconTextCell(style: .default, reuseIdentifier: self.reuseIdentifier())}
    override func reuseIdentifier() -> String {return "IconTextCell"}
    override func nibName() -> String {return "IconTextCell"}

    var iconColor:UIColor?
    var indicatorImageName:String?
    
    func setIndicatorImage(imageName:String) -> ItemIconTextCell {
        self.indicatorImageName = imageName
        return self
    }
    
    func setIconColor(color:UIColor) -> ItemIconTextCell {
        self.iconColor = color
        return self
    }
}

class IconTextCell: BaseCell {
    func castItem() -> ItemIconTextCell { return cellItem as! ItemIconTextCell}
    
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var icon: UIImageView?
    @IBOutlet weak var indicator: UIImageView?
    @IBOutlet weak var iconWidthConstraint: NSLayoutConstraint!
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.titleLabel?.textColor = UIColor(red: 53, green: 64, blue: 82)
        self.titleLabel?.text = cellItem.title
        
        if let imageName = cellItem.imageName{
            self.icon?.image = UIImage(named: imageName)
            self.iconWidthConstraint.constant = 25
        }else{
            self.icon?.image = nil
            self.iconWidthConstraint.constant = 0
        }
        if let iconColor = castItem().iconColor {
            self.icon?.setImageColor(color: iconColor)
        }
        
        if let indicatorImageName = castItem().indicatorImageName{
            self.indicator?.image = UIImage(named: indicatorImageName)
        }else{
            self.indicator?.image = nil
        }
//        self.indicator?.setImageColor(color: UIColor.aviraTextGray())
    }
}
