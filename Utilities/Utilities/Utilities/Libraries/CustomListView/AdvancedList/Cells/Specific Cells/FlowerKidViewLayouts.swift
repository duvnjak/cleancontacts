//
//  FlowerKidViewLayouts.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/8/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func styleBoarderedView(){
        self.styleCornerRadius()
        self.layer.borderWidth = 0.5
        //self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderColor = UIColor.clear.cgColor
    }
    
    func styleCornerRadius(){
        self.layer.cornerRadius = 4
    }
}
