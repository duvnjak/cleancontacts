//
//  LargeButtonCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/4/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemLargeButton : Item {
    override func newCell() -> BaseCell {return LargeButtonCell(style: .default, reuseIdentifier: self.reuseIdentifier())}
    override func reuseIdentifier() -> String {return "LargeButtonCell"}
    
    override public init(){
        super.init()
        self.disableCellSelection = true
    }
    
    var buttonTitle:String = ""
    
    func setButtonTitle(title:String) -> ItemLargeButton {
        buttonTitle = title
        return self
    }
}

class LargeButtonCell: BaseCell {
    @IBOutlet weak var boarderView: UIView?
    
    @IBOutlet weak var icon: UIImageView?
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var button: UIButton?
    
    func castItem() -> ItemLargeButton { return cellItem as! ItemLargeButton}
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.setupCellLayout()
        
        self.titleLabel?.text = castItem().title
        self.descriptionLabel?.text = castItem().detail
        self.icon?.image = UIImage(named: cellItem.imageName ?? "") ?? nil
        self.icon?.setImageColor(color: UIColor.white)
        self.button?.setTitle(castItem().buttonTitle, for: .normal)
    }
    
    @IBAction func didToggleButton(_ sender: AnyObject) {
        self.cellItem?.action?()
    }
    
    func setupCellLayout() {
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        self.boarderView?.styleCornerRadius()
        self.boarderView?.backgroundColor = App.AppColor.FlowerKid.mainColor
    }
}
