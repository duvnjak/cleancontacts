//
//  IconTextCell2.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 6/27/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemIconText2 : Item {
    override func newCell() -> BaseCell {return IconTextCell2(style: .default, reuseIdentifier: self.reuseIdentifier())}
    override func reuseIdentifier() -> String {return "IconTextCell2"}
}

class IconTextCell2: BaseCell {
    @IBOutlet weak var boarderView: UIView?
    @IBOutlet weak var icon: UIImageView?
    @IBOutlet weak var descriptionLabel: UILabel?
    
    func castItem() -> ItemIconText2 { return cellItem as! ItemIconText2}
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.setupCellLayout()
        
        self.descriptionLabel?.text = castItem().title
        self.icon?.image = UIImage(named: castItem().imageName ?? "") ?? nil
        if let tintColor = cellItem.tintColor{
            self.icon?.setImageColor(color: tintColor)
            self.descriptionLabel?.textColor = tintColor
        }
    }
    
    @IBAction func didToggleButton(_ sender: AnyObject) {
        self.cellItem?.action?()
    }
    
    func setupCellLayout() {
        self.descriptionLabel?.textColor = App.AppColor.FlowerKid.textBlack
        
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        self.boarderView?.styleBoarderedView()
        self.boarderView?.backgroundColor = App.AppColor.FlowerKid.grayBGColor
    }
}
