//
//  ProfileCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/8/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemProfile : Item {
    override func newCell() -> BaseCell {return ProfileCell(style: .default, reuseIdentifier: self.reuseIdentifier())}
    override func reuseIdentifier() -> String {return "ProfileCell"}
}

class ProfileCell: BaseCell {
    @IBOutlet weak var icon: UIImageView?
    @IBOutlet weak var titleLabel: UILabel?
    
    func castItem() -> ItemProfile { return cellItem as! ItemProfile}
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.titleLabel?.text = castItem().title
        self.titleLabel?.textColor = App.AppColor.FlowerKid.textBlack

        self.icon?.image = UIImage(named: cellItem.imageName ?? "") ?? nil
    }
}

