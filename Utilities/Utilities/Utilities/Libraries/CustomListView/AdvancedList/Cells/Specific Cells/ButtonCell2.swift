//
//  ButtonCell2.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/8/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemButton2 : Item {
    override func reuseIdentifier() -> String {return "ButtonCell2"}
    override func newCell() -> BaseCell {return ButtonCell2(style: .default, reuseIdentifier: self.reuseIdentifier())}
}

class ButtonCell2: BaseCell {
    func castItem() -> ItemButton2 { return cellItem as! ItemButton2}
    @IBOutlet weak var button: UIButton!
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        self.button?.setTitle(castItem().title, for: .normal)
    }
    
    @IBAction func didToggleButton(_ sender: AnyObject) {
        self.cellItem?.action?()
    }
}
