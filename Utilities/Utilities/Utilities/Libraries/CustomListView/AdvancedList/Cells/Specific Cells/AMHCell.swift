//
//  AMHCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/22/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class AMHItem : Item {
    override func reuseIdentifier() -> String {return "AMHCell"}
    override func newCell() -> BaseCell {return AMHCell(style: .default, reuseIdentifier: self.reuseIdentifier())}
    
    var level:Int = 0
    var AMH:Double = 0
    var age:Int = 0


    
    override init(){
        super.init()
        forcedHeight = 300
    }
    
    func setAMH(amh:Double, age:Int) -> AMHItem {
        self.AMH = amh
        self.age = age
        self.liveCellUpdateHandler?(true)
        return self
    }
    
    func setLevel(level:Int) -> AMHItem {
        self.level = level
        self.liveCellUpdateHandler?(true)
        return self
    }
}

class AMHCell: BaseCell {
    func castItem() -> AMHItem { return cellItem as! AMHItem}
    
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var image4: UIImageView!
    @IBOutlet weak var image5: UIImageView!
    @IBOutlet weak var image6: UIImageView!
    @IBOutlet weak var image7: UIImageView!
    
    @IBOutlet weak var labelBig: UILabel!
    @IBOutlet weak var labelSmall: UILabel!
    
    @IBOutlet weak var labelHighIndicator: UILabel!
    @IBOutlet weak var labelLowIndicator: UILabel!
    
    var allGraphPoints:[UIImageView]?
    
//    override public func updateCell(forCellItem cellItem:Item) {
//        super.updateCell(forCellItem: cellItem)
//        
//        allGraphPoints = [image1, image2, image3, image4, image5, image6, image7]
//        image1.tag = AMHFertility.veryLow.rawValue
//        image2.tag = AMHFertility.low.rawValue
//        image3.tag = AMHFertility.littleBelowNormal.rawValue
//        image4.tag = AMHFertility.normal.rawValue
//        image5.tag = AMHFertility.littleAboveNormal.rawValue
//        image6.tag = AMHFertility.high.rawValue
//        image7.tag = AMHFertility.veryHigh.rawValue
//        
//        self.hideAllPoints()
//        let percentile = AMHPercentileValues.percentailForAMH(years: castItem().age, AMH: castItem().AMH)
//        let amhFertility = AMHPercentileValues.amhFertility(lowerPercentile: percentile.low)
//        self.setLevel(level: amhFertility.rawValue)
//        
//        labelBig.text = "\(castItem().AMH.rounded(toPlaces:2))"
//        labelSmall.text = "ng/mL"
//        
//        self.styleLabelIndicator(label: labelLowIndicator)
//        labelLowIndicator.text = "low".localized.uppercased()
//        
//        self.styleLabelIndicator(label: labelHighIndicator)
//        labelHighIndicator.text = "high".localized.uppercased()
//    }
    
    func styleLabelIndicator(label:UILabel) {
        label.backgroundColor = App.AppColor.FlowerKid.veryLowVeryHigh
        label.layer.cornerRadius = 3
        label.clipsToBounds = true
        label.textColor = UIColor.white
    }
    
    func hideAllPoints() {
        for image in allGraphPoints!{
            image.isHidden = true
        }
    }
    
    func setLevel(level:Int) {
        hideAllPoints()
//        imageViewForLevel(level: level)?.isHidden = false
    }
    
//    func imageViewForLevel(level:Int) -> UIImageView? {
//        return self.viewForTag(tag: level, views: allGraphPoints!)
//    }
}

