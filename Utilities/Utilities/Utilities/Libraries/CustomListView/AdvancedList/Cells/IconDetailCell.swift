//
//  IconDetailCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/9/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemIconDetail : Item {
    override func newCell() -> BaseCell {return IconDetailCell(item: self)}
    override func reuseIdentifier() -> String {return "IconDetailCell"}
    
    var iconColor:UIColor?
    var indicatorImageName:String?
    
    func setIndicatorImage(imageName:String) -> Self {
        self.indicatorImageName = imageName
        return self
    }
    
    func setIconColor(color:UIColor) -> Self {
        self.iconColor = color
        return self
    }
}

class IconDetailCell: BaseCell {
    override class func reuseIdentifier() -> String { return "IconDetailCell" }

    func castItem() -> ItemIconDetail { return cellItem as! ItemIconDetail}

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var icon: UIImageView?
    @IBOutlet weak var indicator: UIImageView?
    @IBOutlet weak var iconWidthConstraint: NSLayoutConstraint!
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.titleLabel?.textColor = App.AppColor.FlowerKid.textBlack
        self.titleLabel?.text = cellItem.title
        
        self.descriptionLabel?.textColor = App.AppColor.FlowerKid.mainColor
        self.descriptionLabel?.text = cellItem.detail
        
        if let imageName = cellItem.imageName{
            self.icon?.image = UIImage(named: imageName)
            self.iconWidthConstraint.constant = 25
        }else{
            self.icon?.image = nil
            self.iconWidthConstraint.constant = 0
        }
        if let iconColor = castItem().iconColor {
            self.icon?.setImageColor(color: iconColor)
        }
        
        if let indicatorImageName = castItem().indicatorImageName{
            self.indicator?.image = UIImage(named: indicatorImageName)
        }else{
            self.indicator?.image = nil
        }
        self.indicator?.setImageColor(color: App.AppColor.FlowerKid.grayIcon)

        
    }
}

