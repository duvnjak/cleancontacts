//
//  DetailCell2.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/7/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemDetail2 : Item {
    override func newCell() -> BaseCell {return DetailCell2(item: self)}
    override func reuseIdentifier() -> String {return "DetailCell2"}
}

class DetailCell2: BaseCell {
    override class func reuseIdentifier() -> String { return "DetailCell2" }

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.titleLabel?.text = cellItem.title
        self.descriptionLabel?.text = cellItem.detail
    }
}
