//
//  CollectionCellSimpleImage.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/17/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class CollectionCellSimpleImageItem : CollectionItem {
    override func cellClass() -> UICollectionViewCell.Type { return CollectionCellSimpleImage.self }
    override func reuseIdentifier() -> String {return "CollectionCellSimpleImage"}
    
    var color:UIColor = UIColor.clear

    func setColor(color:UIColor) -> CollectionCellSimpleImageItem {
        self.color = color
        self.backgroundColor = UIColor.clear
        return self
    }
}

class CollectionCellSimpleImage: BaseCollectionCell {
    func castItem() -> CollectionCellSimpleImageItem { return cellItem as! CollectionCellSimpleImageItem}
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!

    override func cellDidLoad() {
        super.cellDidLoad()
        
        self.containerView.styleCornerRadius()
        
        self.containerView.layer.cornerRadius = 10
        self.containerView.layer.backgroundColor = UIColor.lightGray.lighter()?.cgColor

    }
    
    override public func updateCell(forCellItem cellItem:CollectionItem, animated:Bool = false) {
        super.updateCell(forCellItem: cellItem, animated:animated)
        
        self.titleLabel.text = castItem().title
        self.containerView.backgroundColor = castItem().color
        self.cellImage.image = UIImage(named: castItem().imageName ?? "") ?? nil
        
        if let tintColor = castItem().tintColor {
            self.cellImage.setImageColor(color: tintColor)
            self.titleLabel.textColor = tintColor
        }
    }
}
