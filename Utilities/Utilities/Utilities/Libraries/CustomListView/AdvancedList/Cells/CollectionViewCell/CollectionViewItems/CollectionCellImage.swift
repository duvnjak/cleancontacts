//
//  CollectionCellImage.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/4/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class CollectionCellImageItem : CollectionItem {
    override func cellClass() -> UICollectionViewCell.Type { return CollectionCellImage.self }
    override func reuseIdentifier() -> String {return "CollectionCellImage"}
}

class CollectionCellImage: BaseCollectionCell {
    func castItem() -> CollectionCellImageItem { return cellItem as! CollectionCellImageItem}
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func cellDidLoad() {
        super.cellDidLoad()

        self.containerView.layer.cornerRadius = 10
        self.containerView.styleCornerRadius()
        self.containerView.clipsToBounds = true
    }
    
    override public func updateCell(forCellItem cellItem:CollectionItem, animated:Bool = false) {
        super.updateCell(forCellItem: cellItem, animated:animated)
                
        self.titleLabel.text = castItem().title
        self.detailLabel.text = castItem().detail
        self.cellImage.image = UIImage(named: castItem().imageName ?? "") ?? nil
        self.containerView.backgroundColor = UIColor.white
    }
}
