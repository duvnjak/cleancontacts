//
//  BaseCollectionCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/4/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class CollectionItem {
    func cellClass() -> UICollectionViewCell.Type { return BaseCollectionCell.self }
    func reuseIdentifier() -> String {return "reuse_identifier_collection_cell"}
    func nibName() -> String {return self.reuseIdentifier()}

    var liveCellUpdateHandler: ((_ animated: Bool)->())?
    
    var fontSize = 0;
    var fontBold = false
    var backgroundColor:UIColor? = nil
    var tintColor:UIColor? = nil
    var rowId: String? = nil
    var title: String?
    var detail: String?
    var detail2: String?

    var imageName: String? = nil
    var userInfo: Any? = []
    
    //Actions
    var action: (()->())?
    var deleteAction: (()->())? = nil
    var didDisplayAction: (()->())? = nil
    var didHideAction: (()->())? = nil
    
    public init(){
        
    }
    
    convenience init(rowId:String? = nil, title: String = "", detail: String = "", imageName: String? = nil, userInfo: Any? = nil) {
        self.init()
        self.rowId = rowId
        self.title = title
        self.detail = detail
        self.imageName = imageName
    }
    
    func setTintColor(color:UIColor) -> CollectionItem  {
        self.tintColor = color
        self.liveCellUpdateHandler?(false)
        return self
    }
    
    func setTitle(title:String, animated:Bool = false) -> CollectionItem  {
        self.title = title
        self.liveCellUpdateHandler?(animated)
        return self
    }
    
    func setDetail(detail:String, animated:Bool = false) -> CollectionItem  {
        self.detail = detail
        self.liveCellUpdateHandler?(animated)
        return self
    }
    
    func setImageName(imageName:String, animated:Bool = false) -> CollectionItem  {
        self.imageName = imageName
        self.liveCellUpdateHandler?(animated)
        return self
    }
    
    func onDelete(delete:@escaping (()->(Void))) -> CollectionItem {
        self.deleteAction = delete
        return self
    }
    
    func onAction(action:@escaping (()->(Void))) -> CollectionItem {
        self.action = action
        return self
    }
    
    func onDidDisplay(action:@escaping (()->(Void))) -> CollectionItem {
        self.didDisplayAction = action
        return self
    }
    
    func onDidHide(action:@escaping (()->(Void))) -> CollectionItem {
        self.didHideAction = action
        return self
    }
}

class BaseCollectionCell: UICollectionViewCell {
    var cellItem:CollectionItem?
    var didLoad = false
    
    public func updateCell(forCellItem cellItem:CollectionItem) {
        self.updateCell(forCellItem: cellItem, animated:false)
    }
    
    public func cellDidLoad(){
        //override if needed
    }
    
    public func updateCell(forCellItem cellItem:CollectionItem, animated:Bool = false) {
        self.cellItem = cellItem
        
        if !self.didLoad {
            self.didLoad = true
            self.cellDidLoad()
        }
        
        if let backgroundColor = cellItem.backgroundColor {
            self.contentView.backgroundColor = backgroundColor
        }else{
            self.contentView.backgroundColor = UIColor.clear
        }
        
        self.cellItem?.liveCellUpdateHandler = { animated in
            self.updateCell(forCellItem: self.cellItem!, animated:animated)
        }
    }
    
}
