//
//  LoadingCollectionCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 6/25/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit
import CircleProgressView

class LoadingCollectionCellItem : CollectionItem {
    override func cellClass() -> UICollectionViewCell.Type { return LoadingCollectionCell.self }
    override func reuseIdentifier() -> String {return "LoadingCollectionCell"}
    
    var color:UIColor = UIColor.clear
    var progress:Double = 0
    
    func setColor(color:UIColor) -> LoadingCollectionCellItem {
        self.color = color
        self.backgroundColor = UIColor.clear
        return self
    }
    
    func updateProgress(progress:Double) -> LoadingCollectionCellItem {
        self.progress = progress
        self.liveCellUpdateHandler?(false)
        return self
    }
}

class LoadingCollectionCell: BaseCollectionCell {
    func castItem() -> LoadingCollectionCellItem { return cellItem as! LoadingCollectionCellItem}
    
    @IBOutlet weak var progressView: CircleProgressView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
//    var smoothProgressTimer:SmoothProgressTimer = SmoothProgressTimer()
    
    override func cellDidLoad() {
        super.cellDidLoad()
        
        self.containerView.styleCornerRadius()
        
        self.containerView.layer.cornerRadius = 10
        self.containerView.layer.backgroundColor = UIColor.lightGray.lighter()?.cgColor
    }
    
    override public func updateCell(forCellItem cellItem:CollectionItem, animated:Bool = false) {
        super.updateCell(forCellItem: cellItem, animated:animated)
        
        self.titleLabel.text = castItem().title
        self.containerView.backgroundColor = castItem().color
        
        self.progressView.setProgress(castItem().progress, animated: false)
    }
}
