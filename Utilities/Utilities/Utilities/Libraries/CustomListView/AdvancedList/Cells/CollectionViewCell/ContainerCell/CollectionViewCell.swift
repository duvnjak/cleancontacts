//
//  CollectionViewCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/4/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemCollection : Item {
    override func newCell() -> BaseCell {return CollectionViewCell(style: .default, reuseIdentifier: self.reuseIdentifier())}
    override func reuseIdentifier() -> String {return "CollectionViewCell"}
    
    var collectionItems:[CollectionItem] = []
    var estimatedCellSize = CGSize(200, 200)
    
    func setEstimatedCellSize(size:CGSize) -> Self {
        self.estimatedCellSize = size
        self.forcedHeight = size.height + 1
        return self
    }
    
    func setCollectionItems(collectionItems:[CollectionItem]) -> Self {
        self.collectionItems = collectionItems
        self.liveCellUpdateHandler?(false)
        return self
    }
}

class CollectionViewCell: BaseCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionView: UICollectionView?
    
    func castItem() -> ItemCollection { return cellItem as! ItemCollection}
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.updateViewConstraints()
        self.setupCellLayout()
        
        self.collectionView?.backgroundColor = UIColor.clear
        self.collectionView?.backgroundView = UIView(frame: CGRect.zero)
        
        self.collectionView?.reloadData()
        self.layoutIfNeeded()
    }
    
    func register(collectionItem:CollectionItem) {
        collectionView?.register(UINib(nibName: collectionItem.nibName(), bundle: nil), forCellWithReuseIdentifier: collectionItem.reuseIdentifier())
    }
    
    func updateViewConstraints() {
        let item = castItem()
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
            flowLayout.scrollDirection = .horizontal;
            flowLayout.estimatedItemSize = CGSize(item.estimatedCellSize.width - 0.5, item.estimatedCellSize.height - 0.5)
            flowLayout.minimumLineSpacing = 0
            flowLayout.minimumInteritemSpacing = 0
        }
    }
    
    override public func cellDidLoad(){
        super.cellDidLoad()
        
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.alwaysBounceVertical = false
        collectionView?.alwaysBounceHorizontal = true
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.clipsToBounds = false
        
        for item in allCollectionItems(){
            self.register(collectionItem: item)
        }
    }
    
    func allCollectionItems() -> [CollectionItem] {
        return [
            CollectionCellTextsItem(),
            CollectionCellImageItem(),
            CollectionCellSimpleImageItem(),
            CollectionCellImageDetailItem(),
            LoadingCollectionCellItem()
        ]
    }
    
    func setupCellLayout() {
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return castItem().collectionItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionItem = castItem().collectionItems[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionItem.reuseIdentifier(), for: indexPath as IndexPath) as! BaseCollectionCell
        cell.updateCell(forCellItem: collectionItem, animated: false)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return castItem().estimatedCellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        let collectionItem = castItem().collectionItems[indexPath.row]
        return collectionItem.action != nil
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let collectionItem = castItem().collectionItems[indexPath.row]
        collectionItem.action?()
    }
}

