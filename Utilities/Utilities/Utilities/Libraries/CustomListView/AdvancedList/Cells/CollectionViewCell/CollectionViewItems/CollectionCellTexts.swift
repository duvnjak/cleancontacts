//
//  CollectionCellTexts.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/4/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class MeasurementItem : CollectionCellTextsItem {
//    convenience init(measurement:UserMeasurement, calculations:ReportCalculations) {
//        self.init()
//        self.title = measurement.typeEnum.detailDescription()
//        self.detail = measurement.dateString().uppercased()
//        self.detail2 = "\(measurement.result) ng/mL"
//        self.detail3 = calculations.amhFertilityDescription
//        self.tintColor = calculations.amhFertilityColor
//        
//        self.color = UIColor.white
//    }
}


class CollectionCellTextsItem : CollectionItem {
    override func cellClass() -> UICollectionViewCell.Type { return CollectionCellTexts.self }
    override func reuseIdentifier() -> String {return "CollectionCellTexts"}
    
    var color:UIColor = UIColor.clear
    
    var detail3:String = ""

    func setColor(color:UIColor) -> CollectionCellTextsItem {
        self.color = color
        self.backgroundColor = UIColor.clear
        return self
    }
}

class CollectionCellTexts: BaseCollectionCell {
    func castItem() -> CollectionCellTextsItem { return cellItem as! CollectionCellTextsItem}
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var detail3Label: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mainButton: UIButton!
    
    override func cellDidLoad() {
        super.cellDidLoad()
        
        self.mainButton.isUserInteractionEnabled = false
        
        self.containerView.styleCornerRadius()
        
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
    }

    override public func updateCell(forCellItem cellItem:CollectionItem, animated:Bool = false) {
        super.updateCell(forCellItem: cellItem, animated:animated)
        
        self.titleLabel.text = castItem().title
        self.detailLabel.text = castItem().detail
        self.detail3Label.text = castItem().detail2
        self.mainButton.setTitle(castItem().detail3, for: .normal)
        self.containerView.backgroundColor = castItem().color
        
        if let tintColor = castItem().tintColor{
            self.mainButton.layoutIfNeeded()
            self.mainButton.backgroundColor = tintColor
        }
    }
}
