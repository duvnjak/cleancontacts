//
//  CollectionCellImageDetail.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 6/13/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class CollectionCellImageDetailItem : CollectionItem {
    override func cellClass() -> UICollectionViewCell.Type { return CollectionCellImageDetail.self }
    override func reuseIdentifier() -> String {return "CollectionCellImageDetail"}
    
    var color:UIColor = UIColor.clear
    
    var detail3:String = ""
    
    convenience init(name:String? = "", description:String? = "", price:String? = "", image:String? = nil) {
        self.init()
        self.imageName = image
        self.title = name
        self.detail = description
        self.detail2 = price
        self.color = UIColor.white
    }
}


class CollectionCellImageDetail: BaseCollectionCell {
    func castItem() -> CollectionCellImageDetailItem { return cellItem as! CollectionCellImageDetailItem}
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var detailLabel2: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    override func cellDidLoad() {
        super.cellDidLoad()
        
        self.containerView.styleCornerRadius()
        
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
    }
    
    override public func updateCell(forCellItem cellItem:CollectionItem, animated:Bool = false) {
        super.updateCell(forCellItem: cellItem, animated:animated)
        
        self.imageView.image = UIImage(named: castItem().imageName ?? "") ?? nil
        self.titleLabel.text = castItem().title
        self.detailLabel.text = castItem().detail
        self.detailLabel2.text = castItem().detail2
        self.containerView.backgroundColor = castItem().color
    }
}
