//
//  ReadMoreCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/5/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class ReadMoreCell: BaseCell {
    @IBOutlet weak var customLabel: UILabel?
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.customLabel?.text = cellItem.title
    }
}
