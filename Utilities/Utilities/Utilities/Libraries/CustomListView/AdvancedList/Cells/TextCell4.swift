//
//  TextCell4.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/8/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemText4 : Item {
    override func newCell() -> BaseCell {return TextCell4(item: self)}
    override func reuseIdentifier() -> String {return "TextCell4"}
}

class TextCell4: BaseCell {
    override class func reuseIdentifier() -> String { return "TextCell4" }

    @IBOutlet weak var customLabel: UILabel?
    @IBOutlet weak var customLabel2: UILabel?

    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        
        self.customLabel?.textColor = App.AppColor.FlowerKid.textBlack
        self.customLabel2?.textColor = App.AppColor.FlowerKid.textBlack

        self.customLabel?.text = cellItem.title
        self.customLabel2?.text = cellItem.detail
    }
}
