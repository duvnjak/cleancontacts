//
//  TextCellTitle2.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/10/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemTextTitle2BoldLeftColored : ItemTextTitle2Bold {
    override public init(){
        super.init()
        self.textAlignment = .left
        self.tintColor = App.AppColor.FlowerKid.mainColor
        self.fontSize = 30
    }
    
    func showDownArrow(show:Bool) -> Self {
        if show {
            self.imageName = "icon_down"
        }else{
            self.imageName = "icon_up"
        }
        return self
    }
}

public class ItemTextTitle2BoldCenteredSmall : ItemTextTitle2BoldCentered {
    override public init(){
        super.init()
        self.fontSize = 16
    }
}

public class ItemTextTitle2BoldCentered : ItemTextTitle2Bold {
    override public init(){
        super.init()
        self.textAlignment = .center
    }
}

public class ItemTextTitle2Bold : ItemTextTitle2 {
    override public init(){
        super.init()
        self.fontBold = true
        self.fontSize = 25
    }
}

public class ItemTextTitle2 : Item {
    override func newCell() -> BaseCell {return TextCellTitle2(item: self)}
    override func reuseIdentifier() -> String {return "TextCellTitle2"}
        
    override public init(){
        super.init()
        self.fontBold = true
        self.fontSize = 20
    }
}

class TextCellTitle2: BaseCell {
    override class func reuseIdentifier() -> String { return "TextCellTitle2" }

    @IBOutlet weak var customLabel: UILabel?
    @IBOutlet weak var customImageView: UIImageView?
    
    func castItem() -> ItemTextTitle2 { return cellItem as! ItemTextTitle2}
    
    override public func updateCell(forCellItem cellItem:Item, animated:Bool = false) {
        super.updateCell(forCellItem: cellItem, animated: animated)
        
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        
        self.customLabel?.text = cellItem.title
        
        if (cellItem.fontSize != 0){
            self.customLabel?.font = UIFont.systemFont(ofSize: CGFloat(cellItem.fontSize))
        }
        if (cellItem.fontBold){
            self.customLabel?.font = UIFont.systemFont(ofSize: (self.customLabel?.font.pointSize)!, weight: .heavy)
        }else{
            self.customLabel?.font = UIFont.systemFont(ofSize: (self.customLabel?.font.pointSize)!)
        }
        
        if let tintColor = castItem().tintColor {
            self.customLabel?.textColor = tintColor
        }else{
            self.customLabel?.textColor = UIColor.black
        }
        
        if let imageName = castItem().imageName{
            self.customImageView?.image = UIImage(named: imageName) ?? nil
        }else{
            self.customImageView?.image = nil
        }
        
        if let textAlignment = castItem().textAlignment{
            self.customLabel?.textAlignment = textAlignment
        }
    }
}
