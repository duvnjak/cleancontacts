//
//  TitleDescriptionButtonCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/25/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemTitleDescriptionButton : Item {
    override func newCell() -> BaseCell {return TitleDescriptionButtonCell(item: self)}
    override func reuseIdentifier() -> String {return "TitleDescriptionButtonCell"}
}

class TitleDescriptionButtonCell: BaseCell {
    override class func reuseIdentifier() -> String { return "TitleDescriptionButtonCell" }

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var button: UIButton!
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.titleLabel?.text = cellItem.title
        self.descriptionLabel?.text = cellItem.detail
    }
    
    @IBAction func didToggleButton(_ sender: AnyObject) {
        self.cellItem?.action?()
    }
}
