//
//  ButtonLabelCell.swift
//  Avira Cleaner
//
//  Created by Filip Duvnjak on 8/21/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemButtonLabelCell : Item {
    override func newCell() -> BaseCell {return ButtonLabelCell(item: self)}
    override func reuseIdentifier() -> String {return "ButtonLabelCell"}
    override func nibName() -> String {return "ButtonLabelCell"}
    
    override public init(){
        super.init()
        
        self.disableCellSelection = true
    }
}

class ButtonLabelCell: BaseCell {
    func castItem() -> ItemButtonLabelCell { return cellItem as! ItemButtonLabelCell}
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var customImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.titleLabel.text = castItem().title
        self.button?.setTitle(castItem().detail, for: .normal)
    }
    
    @IBAction func didToggleButton(_ sender: AnyObject) {
        self.cellItem?.action?()
    }
}

