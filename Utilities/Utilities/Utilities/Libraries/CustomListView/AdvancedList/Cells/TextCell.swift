//
//  TextCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/4/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemTextEmpty : ItemText{
    override public init(){
        super.init()
        self.title = "   ";
    }
}

public class ItemTextTitle : ItemText {
    override public init(){
        super.init()
        self.fontSize = 24
    }
}

public class ItemText : Item {
    override func newCell() -> BaseCell {return TextCell(item: self)}
    override func reuseIdentifier() -> String {return "TextCell"}

    convenience init?(title: String, detail: String, fontSize: Int) {
        self.init(title: title, detail: detail)
        self.fontSize = fontSize
    }
}

class TextCell: BaseCell {
    override class func reuseIdentifier() -> String { return "TextCell" }

    @IBOutlet weak var customLabel: UILabel?
    
    override public func updateCell(forCellItem cellItem:Item, animated:Bool = false) {
        super.updateCell(forCellItem: cellItem, animated: animated)
        
        self.customLabel?.text = cellItem.title
        if (cellItem.fontSize != 0){
            self.customLabel?.font = UIFont.systemFont(ofSize: CGFloat(cellItem.fontSize))
        }else{
            self.customLabel?.font = UIFont.systemFont(ofSize: 17)
        }
    }
}
