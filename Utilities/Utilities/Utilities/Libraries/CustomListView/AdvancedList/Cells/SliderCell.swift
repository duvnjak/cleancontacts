//
//  SliderCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 4/17/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class SliderItem : Item {
    override func newCell() -> BaseCell {return SliderCell(item: self)}
    override func reuseIdentifier() -> String {return "SliderCell"}

    var updateAction: ((Float)->())?
    var min:Float = 0
    var max:Float = 100
    var value:Float = 0

    func onValueUpdate(updateAction:@escaping ((Float)->())) -> Self {
        self.updateAction = updateAction
        return self
    }
    
    func setMin(min:Float) -> Self  {
        self.min = min
        return self
    }
    
    func setMax(max:Float) -> Self  {
        self.max = max
        return self
    }
    
    func setValue(value:Float, animated:Bool = false) -> Self  {
        self.value = value
        self.liveCellUpdateHandler?(animated)
        return self
    }
}

class SliderCell: BaseCell {
    override class func reuseIdentifier() -> String { return "SliderCell" }

    func castItem() -> SliderItem { return cellItem as! SliderItem}

    @IBOutlet weak var slider: UISlider?    
    
    override public func updateCell(forCellItem cellItem:Item, animated:Bool = false) {
        super.updateCell(forCellItem: cellItem, animated: animated)
        
        slider?.minimumValue = castItem().min
        slider?.maximumValue = castItem().max
        
        slider?.setValue(castItem().value, animated: animated)
    }
    
    @IBAction func sliderUpdate(_ sender: UIButton) {
        self.castItem().updateAction?(slider!.value)
    }
    
    
}

