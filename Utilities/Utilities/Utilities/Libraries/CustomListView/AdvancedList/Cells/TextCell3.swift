//
//  TextCell3.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/4/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemText3 : Item {
    override func newCell() -> BaseCell {return TextCell3(item: self)}
    override func reuseIdentifier() -> String {return "TextCell3"}
}

class TextCell3: BaseCell {
    override class func reuseIdentifier() -> String { return "TextCell3" }

    @IBOutlet weak var customLabel: UILabel?
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        self.customLabel?.text = cellItem.title
    }
}



