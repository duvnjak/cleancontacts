//
//  LoadingCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 4/28/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemLoading : Item {
    override func newCell() -> BaseCell {return LoadingCell(item: self)}
    override func reuseIdentifier() -> String {return "LoadingCell"}
}

class LoadingCell: BaseCell {
    override class func reuseIdentifier() -> String { return "LoadingCell" }

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    func castItem() -> ItemLoading { return cellItem as! ItemLoading}

    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.titleLabel.text = castItem().title
        self.activityIndicator.startAnimating()
    }
}
