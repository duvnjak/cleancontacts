//
//  ImageCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/25/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemImage : Item {
    override func newCell() -> BaseCell {return ImageCell(item: self)}
    override func reuseIdentifier() -> String {return "ImageCell"}
}

class ImageCell: BaseCell {
    override class func reuseIdentifier() -> String { return "ImageCell" }

    @IBOutlet weak var customImageView: UIImageView!
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
                
        self.customImageView.image = UIImage(named: cellItem.imageName ?? "") ?? nil
        self.backgroundColor = UIColor.red
    }
}

