//
//  ButtonCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/25/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemButton : Item {
    override func newCell() -> BaseCell {return ButtonCell(item: self)}
    override func reuseIdentifier() -> String {return "ButtonCell"}
}

class ButtonCell: BaseCell {
    override class func reuseIdentifier() -> String { return "ButtonCell" }

    func castItem() -> ItemButton { return cellItem as! ItemButton}
    @IBOutlet weak var button: UIButton!
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        self.button?.setTitle(castItem().title, for: .normal)
    }
    
    @IBAction func didToggleButton(_ sender: AnyObject) {
        self.cellItem?.action?()
    }
}

