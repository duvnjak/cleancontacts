//
//  SeparatorCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/4/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemSeparatorFull: ItemSeparator{
    public override init() {
        super.init()
        forcedHeight = 0.5
        backgroundColor = UIColor.lightGray
        showCustomSeparator = true
    }
}

public class ItemSeparatorClear: ItemSeparator{
    public override init() {
        super.init()
        forcedHeight = 20
        backgroundColor = UIColor.clear
        showCustomSeparator = true
    }
}

public class ItemSeparatorWhite: ItemSeparator{
    public override init() {
        super.init()
        forcedHeight = 20
        backgroundColor = UIColor.white
        showCustomSeparator = true
    }
}

public class ItemSeparatorGray: ItemSeparator{
    public override init() {
        super.init()
        forcedHeight = 20
        backgroundColor = App.AppColor.FlowerKid.grayBGColor
        showCustomSeparator = true
    }
}

public class ItemSeparator : Item {
    override func newCell() -> BaseCell {return SeparatorCell(item: self)}
    override func reuseIdentifier() -> String {return "SeparatorCell"}
    
    convenience init(height:CGFloat = 30, backgroundColor:UIColor = UIColor.white, customSeparator:Bool=false, hideSeparator:Bool = false){
        self.init()
        self.forcedHeight = height
        self.backgroundColor = backgroundColor
        self.showCustomSeparator = customSeparator
        self.hideSeparator = hideSeparator
    }

}

class SeparatorCell: BaseCell {
    override class func reuseIdentifier() -> String { return "SeparatorCell" }

    func castItem() -> ItemSeparator { return cellItem as! ItemSeparator}
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)

    }
}
