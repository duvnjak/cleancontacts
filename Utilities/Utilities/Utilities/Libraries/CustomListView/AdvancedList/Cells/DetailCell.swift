//
//  DetailCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/6/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemDetail3 : Item {
    override func newCell() -> BaseCell {return DetailCell(item: self)}
    override func reuseIdentifier() -> String {return "DetailCell"}
}

class DetailCell: BaseCell {
    override class func reuseIdentifier() -> String { return "DetailCell" }

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?

    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.titleLabel?.text = cellItem.title
        self.descriptionLabel?.text = cellItem.detail

    }
}
