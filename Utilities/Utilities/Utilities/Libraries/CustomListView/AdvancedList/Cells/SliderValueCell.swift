//
//  SliderValueCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 4/27/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class SliderValueItem : SliderItem {
    override func newCell() -> BaseCell {return SliderValueCell(item: self)}
    override func reuseIdentifier() -> String {return "SliderValueCell"}

    var updateActionInternal: ((Float)->())?
    var textValue:String = ""

    override public init(){
        super.init()
        self.updateActionInternal = { value in
            _ = self.setTextValue(value: "\(Int(value))")
            self.updateAction?(value)
        }
    }
    
    func setTextValue(value:String, animated:Bool = false) -> Self  {
        self.textValue = value
        self.liveCellUpdateHandler?(animated)
        return self
    }
   
    override func setValue(value:Float, animated:Bool = false) -> Self  {
        _ = self.setTextValue(value: "\(Int(value))")
        _ = super.setValue(value: value, animated: animated)
        return self
    }
}

class SliderValueCell: BaseCell {
    override class func reuseIdentifier() -> String { return "SliderValueCell" }

    func castItem() -> SliderValueItem { return cellItem as! SliderValueItem}
    
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var valueLabel: UILabel?
    @IBOutlet weak var slider: UISlider?
    
    override public func updateCell(forCellItem cellItem:Item, animated:Bool = false) {
        super.updateCell(forCellItem: cellItem, animated: animated)
        
        slider?.minimumValue = castItem().min
        slider?.maximumValue = castItem().max
        
        slider?.setValue(castItem().value, animated: animated)
    }
    
    @IBAction func sliderUpdate(_ sender: UIButton) {
        //self.castItem().updateActionInternal?(slider!.value)
    }
    
}
