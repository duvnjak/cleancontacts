//
//  SwitchCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/26/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

//Instructions:
//1. add the reusedIdentifier to the nib
//2. add the ItemSwitch to the AdvancedListViewController

public class ItemSwitch2 : Item {
    override func newCell() -> BaseCell {return CustomSwitchCell2(item: self)}
    override func reuseIdentifier() -> String {return "CustomSwitchCell2"}

    var isOn:Bool = false
    var switchAction: ((Bool)->())?
    var willSwitchAction: ((Bool)->())?

    convenience init(title: String, isOn:Bool) {
        self.init(title: title, detail: "")
        self.isOn = isOn
    }
    
    func setIsOn(isOn:Bool, animated:Bool = false) -> Self {
        self.isOn = isOn
        self.liveCellUpdateHandler?(animated)
        return self
    }
    
    func onSwitchAction(switchAction:@escaping ((Bool)->())) -> Self {
        self.switchAction = switchAction
        return self
    }
}

class CustomSwitchCell2: BaseCell {
    override class func reuseIdentifier() -> String { return "CustomSwitchCell2" }

    func castItem() -> ItemSwitch2 { return cellItem as! ItemSwitch2}

    @IBOutlet weak var customSwitch: UISwitch!
    @IBOutlet weak var titleLabel: UILabel?
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.titleLabel?.text = cellItem.title
        self.customSwitch?.isOn = self.castItem().isOn
    }
    
    @IBAction func didToggleButton(_ sender: AnyObject) {
        self.castItem().willSwitchAction?(self.customSwitch.isOn)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            //allow the animation to finish
            self.castItem().switchAction?(self.customSwitch.isOn)
        }
    }
}

