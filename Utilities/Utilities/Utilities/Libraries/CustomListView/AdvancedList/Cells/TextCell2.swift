//
//  TextCell2.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/4/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemText2Bold : ItemText2 {    
    override public init(){
        super.init()
        self.fontBold = true
    }
}

public class ItemFooterSeparatorSmall : ItemFooterSeparator {
    public override init() {
        super.init()
        self.forcedHeight = 5
    }
}

public class ItemFooterSeparator : ItemText2 {
    public override init() {
        super.init()
        self.forcedHeight = 22
        self.title = ""
    }
}

public class ItemText2Attributed : ItemText2 {
    override public init(){
        super.init()
        self.textAlignment = .center
    }
}


public class ItemText2 : Item {
    override func newCell() -> BaseCell {return TextCell2(item: self)}
    override func reuseIdentifier() -> String {return "TextCell2"}
}

class TextCell2: BaseCell {
    override class func reuseIdentifier() -> String { return "TextCell2" }

    @IBOutlet weak var customLabel: UILabel?
    
    func castItem() -> ItemText2 { return cellItem as! ItemText2}
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        
        self.customLabel?.text = cellItem.title
        if (cellItem.fontBold){
            self.customLabel?.font = UIFont.boldSystemFont(ofSize: (self.customLabel?.font.pointSize)!)
        }else{
            self.customLabel?.font = UIFont.systemFont(ofSize: (self.customLabel?.font.pointSize)!)
        }
        
        if let attributed = castItem().attributedTitle {
            self.customLabel?.attributedText = attributed
        }
        
        if let textAlignment = castItem().textAlignment{
            self.customLabel?.textAlignment = textAlignment
        }
    }
}

