//
//  ProgressCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/29/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public class ItemProgress : Item {
    override func newCell() -> BaseCell {return ProgressCell(item: self)}
    override func reuseIdentifier() -> String {return "ProgressCell"}

    var progress:Float = 0.0
    
    func setProgress(percentTo100:Float, animated:Bool = false) -> Self  {
        self.progress = percentTo100
        self.liveCellUpdateHandler?(animated)
        return self
    }
}

class ProgressCell: BaseCell {
    override class func reuseIdentifier() -> String { return "ProgressCell" }

    func castItem() -> ItemProgress { return cellItem as! ItemProgress}

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var customImageView: UIImageView?
    @IBOutlet weak var progressView: UIProgressView?
    
    override public func updateCell(forCellItem cellItem:Item, animated:Bool = false) {
        super.updateCell(forCellItem: cellItem, animated: animated)
                
        self.titleLabel?.text = cellItem.title
        self.descriptionLabel?.text = cellItem.detail
        self.progressView?.setProgress(castItem().progress/100, animated: animated)
    }
}
