//
//  SwitchCell.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/26/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

//Instructions:
//1. add the reusedIdentifier to the nib
//2. add the ItemSwitch to the AdvancedListViewController

public class ItemSwitch : Item {
    override func newCell() -> BaseCell {return CustomSwitchCell(item: self)}
    override func reuseIdentifier() -> String {return "CustomSwitchCell"}
    override func nibName() -> String {return "CustomSwitchCell"}
    
    var isOn:Bool = false
    var switchAction: ((Bool)->())?
    var willSwitchAction: ((Bool)->())?
    
    convenience init(title: String, detail:String, isOn:Bool) {
        self.init(title: title, detail: detail)
        self.isOn = isOn
    }
        
    convenience init(title: String, isOn:Bool) {
        self.init(title: title, detail: "")
        self.isOn = isOn
    }
    
    func willSwitchAction(action:@escaping ((Bool)->())) -> ItemSwitch {
        self.willSwitchAction = switchAction
        return self
    }
    
    func onSwitchAction(switchAction:@escaping ((Bool)->())) -> ItemSwitch {
        self.switchAction = switchAction
        return self
    }
}

class CustomSwitchCell: BaseCell {
    func castItem() -> ItemSwitch { return cellItem as! ItemSwitch}

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var customSwitch: UISwitch!
    
    override public func updateCell(forCellItem cellItem:Item) {
        super.updateCell(forCellItem: cellItem)
        
        self.titleLabel?.text = self.castItem().title
        self.descriptionLabel?.text = self.castItem().detail
        self.customSwitch?.isOn = self.castItem().isOn
        self.customSwitch.tintColor = UIColor(hex: "233455")
        self.customSwitch.onTintColor = UIColor(hex: "233455")
    }
    
    @IBAction func didToggleButton(_ sender: AnyObject) {
        self.castItem().willSwitchAction?(self.customSwitch.isOn)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            //allow the animation to finish
            self.castItem().switchAction?(self.customSwitch.isOn)
        }
    }
}

