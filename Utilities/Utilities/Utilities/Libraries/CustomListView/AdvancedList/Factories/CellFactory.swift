//
//  CellFactory.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/4/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class CellFactory {
    func cell(tableView:UITableView, item:Item) -> BaseCell! {
        let cell = tableView.dequeueReusableCell(withIdentifier: item.reuseIdentifier()) as? BaseCell ?? item.newCell()
        return cell
    }
    
    func header(tableView:UITableView, itemHeader:ItemHeader) -> TableViewHeader! {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: itemHeader.reuseIdentifier()) as? TableViewHeader ?? itemHeader.newHeader()
        return cell
    }
}
