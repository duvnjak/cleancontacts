//
//  CustomCellFactory.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 12/5/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class CustomCellFactory {
    class func testAllCells() -> [Item] {
        return [
            ItemDetail3().setTitle(title: "ItemDetail3").setDetail(detail: "ItemDetail3"),
            ItemCollapsable().setTitle(title: "ItemCollapsable"),
            ItemText().setTitle(title: "ItemText").onAction {
                
            },
            ItemText2().setTitle(title: "ItemText2"),
            ItemText3().setTitle(title: "ItemText3"),
            ItemText4().setTitle(title: "ItemText4"),
            ItemTitleDescriptionButton().setTitle(title: "ItemTitleDescriptionButton").setDetail(detail: "ItemTitleDescriptionButton"),
            ItemImage().setTitle(title: "ItemImage").setImageName(imageName: "AppIcon").setForcedHeight(height: 100),
            ItemSwitch2().setTitle(title: "ItemSwitch2"),
            ItemDetail2().setTitle(title: "ItemDetail2"),
            ItemProgress().setTitle(title: "ItemProgress").setDetail(detail: "ItemProgress"),
            ItemButton().setTitle(title: "ItemButton"),
            SliderItem().setMin(min: 0).setMax(max: 100).setValue(value: 50).setTitle(title: "SliderItem"),
            ItemLoading().setTitle(title: "ItemLoading"),
            
            ItemIconText22().setTitle(title: "ItemIconText").setDetail(detail: "ItemIconText").setImageName(imageName: "AppIcon"),
            ItemIconText2().setTitle(title: "ItemIconText2").setImageName(imageName: "AppIcon"),

            ItemLargeButton().setTitle(title: "ItemLargeButton").setDetail(detail: "ItemLargeButton").setImageName(imageName: "AppIcon"),
            ItemCollection().setTitle(title: "ItemCollection")
                .setEstimatedCellSize(size: CGSize(200, 250))
                .setBackgroundColor(color: UIColor.clear)
                .setCollectionItems(collectionItems: [
                    CollectionCellTextsItem().setTitle(title: "CollectionCellTextsItem").setDetail(detail: "CollectionCellTextsItem"),
                    CollectionCellTextsItem().setTitle(title: "CollectionCellTextsItem").setDetail(detail: "CollectionCellTextsItem"),
                    CollectionCellTextsItem().setTitle(title: "CollectionCellTextsItem").setDetail(detail: "CollectionCellTextsItem")
                    ]),
            ItemSeparator().setTitle(title: "ItemSeparator"),
            ItemSeparatorGray().setTitle(title: "ItemSeparatorGray"),
            ItemSeparatorFull().setTitle(title: "ItemSeparatorGray"),
            ItemSeparatorWhite().setTitle(title: "ItemSeparatorGray"),
            ItemSeparatorGray().setTitle(title: "ItemSeparatorGray"),
            
            SliderValueItem().setTitle(title: "SliderValueItem"),
            ItemButton2().setTitle(title: "ItemButton2"),
            ItemProfile().setTitle(title: "ItemProfile").setImageName(imageName: "AppIcon"),
            ItemIconDetail().setTitle(title: "ItemIconDetail").setDetail(detail: "test").setImageName(imageName: "AppIcon"),
            ItemTextTitle2().setTitle(title: "ItemTextTitle2"),
            AMHItem().setTitle(title: "AMHItem"),
            ItemBigButton().setTitle(title: "ItemBigButton").setImageName(imageName: "AppIcon").setTintColor(color: UIColor.lightGray),
            ItemReviewPurchase().setTitle(title: "ItemReviewPurchase"),
            ItemMenopause().setTitle(title: "ItemMenopause"),
            ItemButtonSmall().setTitle(title: "ItemButtonSmall"),
            ItemIconTextCell().setTitle(title: "ItemIconTextCell"),
            ItemButtonLabelCell(),
            ItemIconText22(),
        ]
    }
    
    class func allCellItems() -> [Item] {
        return [
            ItemDetail3(),
            ItemCollapsable(),
            ItemText2(),
            ItemText(),
            ItemText3(),
            ItemText4(),
            ItemTitleDescriptionButton(),
            ItemImage(),
            ItemSwitch2(),
            ItemDetail2(),
            ItemProgress(),
            ItemButton(),
            SliderItem(),
            ItemLoading(),
            ItemIconText22(),
            ItemLargeButton(),
            ItemCollection(),
            ItemSeparator(),
            SliderValueItem(),
            ItemButton2(),
            ItemProfile(),
            ItemIconDetail(),
            ItemTextTitle2(),
            AMHItem(),
            ItemBigButton(),
            ItemReviewPurchase(),
            ItemMenopause(),
            ItemButtonSmall(),
            ItemIconText2(),
            ItemIconTextCell(),
            ItemButtonLabelCell(),
        ]
    }
    
}
