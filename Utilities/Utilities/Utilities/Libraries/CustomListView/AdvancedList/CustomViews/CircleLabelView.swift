//
//  CircleLabelView.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 6/13/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class CircleLabelView: NibView {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    func setTintColor(color:UIColor) {
        self.label.textColor = color
        self.imageView.setImageColor(color: color)
    }
}
