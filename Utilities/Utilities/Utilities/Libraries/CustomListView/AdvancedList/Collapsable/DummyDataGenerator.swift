//
//  DummyDataGenerator.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/4/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation

class DummyDataGenerator {
    
    public static func generateDummyCells() -> [Item] {
        return
            [
                ItemText(title: "MacBook this is the macbook", detail: ""),
                ItemText2(title: "MacBook Air", detail: "While the screen could be sharper, the updated 11-inch MacBook Air is a very light ultraportable that offers great performance and battery life for the price."),
                
                ItemCollapsable(title: "MacBook Pro", detail: "Retina Display The brightest, most colorful Mac notebook display ever. The display in the MacBook Pro is the best ever in a Mac notebook."),
                
                ItemCollapsable(title: "iMac", detail: "iMac combines enhanced performance with our best ever Retina display for the ultimate desktop experience in two sizes asdf as df asdf  asdf  asdf a sdf as df as df as df asd f a sdf asd f asdf as df as df asdf a dsf as df asdf"),
                
                ItemText3(title: "Mac Pro", detail: "Mac Pro is equipped with pro-level graphics, storage, expansion, processing power, and memory. It's built for creativity on an epic scale."),
                
                ItemCollapsable(title: "Mac mini", detail: "Mac mini is an affordable powerhouse that packs the entire Mac experience into a 7.7-inch-square frame."),
                
                ItemCollapsable(title: "OS X El Capitan", detail: "The twelfth major release of OS X (now named macOS)."),
                
                ItemCollapsable(title: "Accessories", detail: "")
        ]
    }
    
//    public static func deleteAllMeasurements(dbManager:DatabaseManager) {
//        dbManager.activeUser()?.deleteAllMeasurements()
//    }
    
//    public static func generateDummyMeasurements(dbManager:DatabaseManager) {
//        dbManager.activeUser()?.addMeasurement(type: .feritin, result: 10)
//        dbManager.activeUser()?.addMeasurement(type: .feritin, result: 50)
//        dbManager.activeUser()?.addMeasurement(type: .feritin, result: 200)
//        dbManager.activeUser()?.addMeasurement(type: .feritin, result: 1200)
//
//        dbManager.activeUser()?.addMeasurement(type: .crp, result: 2)
//        dbManager.activeUser()?.addMeasurement(type: .crp, result: 8)
//        dbManager.activeUser()?.addMeasurement(type: .crp, result: 10)
//        dbManager.activeUser()?.addMeasurement(type: .crp, result: 30)
//
//        dbManager.activeUser()?.addMeasurement(type: .colesterol, result: 1.1)
//
//        dbManager.activeUser()?.addMeasurement(type: .progestron, result: 1.1)
//
//        dbManager.activeUser()?.addMeasurement(type: .amh, result: 0.1)
//        dbManager.activeUser()?.addMeasurement(type: .amh, result: 0.8)
//        dbManager.activeUser()?.addMeasurement(type: .amh, result: 1.1)
//        dbManager.activeUser()?.addMeasurement(type: .amh, result: 4.7)
//        dbManager.activeUser()?.addMeasurement(type: .amh, result: 4.85)
//        dbManager.activeUser()?.addMeasurement(type: .amh, result: 4.9)
//
//        dbManager.activeUser()?.addMeasurement(type: .amh2, result: 1.0)
//
//    }
    
//    public static func generateMeasurement() {
//        let realm = try! Realm()
//        var user = realm.objects(User.self).first
//        if user == nil {
//            user = User()
//            try! realm.write {
//                realm.add(user!)
//            }
//        }
//        if user?.measurements.isEmpty ?? true {
//            for _ in 1...100 {
//                let measurement = Measurement()
//                measurement.serialNumberLab = UUID().uuidString
//                measurement.serialNumberStrip = UUID().uuidString
//                for _ in 1...100 {
//                    let point = GraphPoint()
//                    point.wavelength = Double(arc4random_uniform(10000)) / 10
//                    point.intensity = Double(arc4random_uniform(10000)) / 1000
//                    try! realm.write {
//                        measurement.metadata.append(point)
//                    }
//                }
//                measurement.amhResult = Double(arc4random_uniform(100)) / 10
//                measurement.timestamp = Date(timeIntervalSince1970: TimeInterval(arc4random_uniform(3_000_000)))
//                measurement.timeTo = measurement.timestamp.addingTimeInterval(50_000)
//                try! realm.write {
//                    user?.measurements.append(measurement)
//                }
//            }
//        } else {
//            
//        }
//    }
    
}

