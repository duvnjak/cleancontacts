//
//  ColapseTableViewConfig.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/4/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation

class CollapsableTableViewConfig {
    var shouldScrollToExpandedSection: Bool = true
    var shouldSetAutomaticSectionHeight: Bool = false
    var shouldCollapseAllOthersOnSelection: Bool = false
}
