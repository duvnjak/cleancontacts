//
//  CollapsableTableViewController.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/4/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class CollapsableTableViewController: AdvancedListViewController{
    
    var config = CollapsableTableViewConfig()
    var sections: [ItemHeader] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorStyle = .none
        
        if config.shouldSetAutomaticSectionHeight {
            tableView.setAutomaticSectionHeight()
        }
    }
    
    override func registerAllNibs() {
        super.registerAllNibs()
        
        self.registerHeaderNibForItem(itemHeader: ItemHeaderCollapsable())
        self.registerHeaderNibForItem(itemHeader: ItemHeaderCollapsableCustom())
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let itemHeader = sections[section] as? ItemHeaderCollapsable{
            return itemHeader.collapsed ? 0 : sections[section].items!.count
        }
        return sections[section].items!.count
    }
    
    // Cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item: Item = sections[indexPath.section].items![indexPath.row]

        let cell: BaseCell = CellFactory().cell(tableView: tableView, item:item)
        cell.updateCell(forCellItem:item)
        
        return cell
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let item: Item = sections[indexPath.section].items![indexPath.row]
        item.action?()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let itemHeader = sections[section]
        
        let header: TableViewHeader = CellFactory().header(tableView: tableView, itemHeader: itemHeader)
        
        header.updateView(forSection:section, sectionModel:sections[section]);
        
        if let collapsabeHeader = header as? CollapsibleTableViewHeader {
            collapsabeHeader.delegate = self
        }
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
}

// MARK: - Section Header Delegate

extension CollapsableTableViewController: CollapsibleTableViewHeaderDelegate {
    
    func toggleSection(_ header: CollapsibleTableViewHeader, section: Int) {
        if let itemHeaderCollapsable = sections[section] as? ItemHeaderCollapsable {
            let collapsed = !itemHeaderCollapsable.collapsed
            
            if (config.shouldCollapseAllOthersOnSelection && !collapsed){
                for i in 0...sections.count - 1 {
                    if let collapsabled = sections[i] as? ItemHeaderCollapsable {
                        collapsabled.collapsed = true
                    }
                }
                itemHeaderCollapsable.collapsed = collapsed
                
                self.tableView.reloadAllSections(.automatic)
            }else{
                itemHeaderCollapsable.collapsed = collapsed
                header.setCollapsed(collapsed)
                tableView.reloadSection(section, with: .automatic)
            }
            
            if (config.shouldScrollToExpandedSection && !collapsed){
                let indexPath = IndexPath(row: 0, section: section)
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
}


