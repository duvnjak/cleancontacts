//
//  HarmonicaTableViewController.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/4/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class HarmonicaTableViewController:CollapsableTableViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //https://stackoverflow.com/questions/20305943/why-extra-space-is-at-top-of-uitableview-simple
        self.automaticallyAdjustsScrollViewInsets = false;
        self.tableView.contentInset = UIEdgeInsets.zero
        self.extendedLayoutIncludesOpaqueBars=false;
    }
}
