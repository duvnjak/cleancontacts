//
//  BlockBarButtonItem.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 4/17/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class BlockBarButtonItem: UIBarButtonItem {
    private var actionHandler: (() -> Void)?
    
    convenience init(title: String?, style: UIBarButtonItemStyle, actionHandler: (() -> Void)?) {
        self.init(title: title, style: style, target: nil, action: #selector(barButtonItemPressed))
        self.target = self
        self.actionHandler = actionHandler
    }
    
    convenience init(image: UIImage?, style: UIBarButtonItemStyle, actionHandler: (() -> Void)?) {
        self.init(image: image, style: style, target: nil, action: #selector(barButtonItemPressed))
        self.target = self
        self.actionHandler = actionHandler
    }
    
    @objc func barButtonItemPressed(sender: UIBarButtonItem) {
        actionHandler?()
    }
}

//let tempVariableIWantToReference = "Classy"
//
//navigationTitle.leftBarButtonItem = BlockBarButtonItem.init(
//    title: "< Back", style: UIBarButtonItemStyle.Plain,
//    actionHandler: { () -> Void in
//        print("Hey I'm in a closure")
//        print("I can even reference temporary variables. \(self.tempVariableIWantToReference)!!!")
//})

