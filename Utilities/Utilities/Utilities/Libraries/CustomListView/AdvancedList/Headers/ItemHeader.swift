//
//  ItemHeader.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/29/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class ItemHeader {
    func newHeader() -> TableViewHeader {return TableViewHeader(reuseIdentifier: self.reuseIdentifier())}
    func reuseIdentifier() -> String {return "TableViewHeader"}
    func nibName() -> String {return self.reuseIdentifier()}
    
    var title: String?
    var items: [Item]?
    
    public init(title: String = "", items: [Item] = []) {
        self.title = title
        self.items = items
    }
}

class TableViewHeader: UITableViewHeaderFooterView {
    var itemHeader:ItemHeader!
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func updateView(forSection section:Int, sectionModel:ItemHeader) {
        self.itemHeader = sectionModel
        
    }
}
