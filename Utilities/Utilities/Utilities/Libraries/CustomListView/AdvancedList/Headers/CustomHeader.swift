//
//  CustomHeader.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/5/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class ItemHeaderCollapsableCustom: ItemHeaderCollapsable {
    override func newHeader() -> CustomHeader {return CustomHeader(reuseIdentifier: self.reuseIdentifier())}
    override func reuseIdentifier() -> String {return "CustomHeader"}
}

class CustomHeader: CollapsibleTableViewHeader {
    @IBOutlet weak var titelLabel: UILabel!

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateView(forSection section:Int, sectionModel:ItemHeader) {
        super.updateView(forSection: section, sectionModel: sectionModel)
        
        self.contentView.backgroundColor = UIColor.white
        self.titelLabel.text = sectionModel.title
    }
    
}
