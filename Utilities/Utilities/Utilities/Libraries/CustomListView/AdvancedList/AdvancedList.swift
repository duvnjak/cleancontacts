//
//  AdvancedListWrapper.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/4/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class AdvancedListFactory {
    public static func createFullScreen(withModel model:[Item]) -> AdvancedListViewController {
        let viewController = AdvancedListViewController.initFullscreen()
        viewController.cells = model
        return viewController
    }
    
    public static func create(withModel model:[Item], backAction: (() -> Void)? = nil) -> AdvancedList {
        let viewController = AdvancedList.initFromStoryboard();
        viewController.cells = model
        viewController.backAction = backAction
        return viewController
    }
}

class AdvancedList: AdvancedListViewController {
    static let identifier = "AdvancedList"
    
    static func initFromStoryboard() -> AdvancedList {
        return UIStoryboard.viewController(storyBoardName: "AdvancedList", identifier: self.identifier) as! AdvancedList
    }
    
    @IBOutlet weak var backButton: UIButton?
    var backAction: (()->(Void))?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.backButton?.isHidden = (self.backAction == nil)
    }
    
    @IBAction func backToggled(_ sender: AnyObject) {
        self.backAction?()
    }
}
