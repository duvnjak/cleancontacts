//
//  ListModalExtension.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 4/27/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    func presentModal(items:[Item], completion:(()->())? = nil) {
        let viewController = AdvancedListViewController.initFullscreen()
        viewController.onViewDidLoad = {
            viewController.tableView.separatorStyle = .none
        }
        viewController.cells = items
        viewController.navigationItem.rightBarButtonItem = BlockBarButtonItem(
            title: "close".localized, style: .plain,
            actionHandler: { viewController.dismiss(animated: true)}
        )
        let navController: UINavigationController = UINavigationController(rootViewController: viewController)
        self.present(navController, animated: true, completion: completion)
    }
}
