//
//  AdvancedListViewController.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/4/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

class AdvancedListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    static func initFullscreen() -> AdvancedListViewController {
        return AdvancedListViewController(nibName: "FullscreenListViewController", bundle: nil)
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    var cells: [Item] = []
    var onViewDidLoad:(()->())?
    var onViewWillAppear:(()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerAllNibs()

        tableView.setAutomaticCellHeight()
        tableView.removeExtraEmptyCells()
        tableView.deselectSelectedRow()
        
        onViewDidLoad?()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        onViewWillAppear?()
    }
    
    //MARK: override to add custom cells
    func registerAllNibs() {
        for item in CustomCellFactory.allCellItems() {
            self.registerNibForItem(item: item)
        }
    }

    
    func scrollToRowId(rowId:String, animated:Bool) {
        let index = cells.index(where: { (item) -> Bool in
            item.rowId == rowId
        })
       self.tableView.scrollToRow(at: IndexPath(row: index!, section: 0), at:.top, animated: animated)

    }
    
    func registerNibForItem(item:Item) {
        if item.nibName() != "" {
            tableView.register(UINib(nibName: item.nibName(), bundle: nil), forCellReuseIdentifier: item.reuseIdentifier())
        }
    }
    
    func registerHeaderNibForItem(itemHeader:ItemHeader) {
        if itemHeader.nibName() != "" {
            tableView.register(UINib(nibName: itemHeader.nibName(), bundle: nil), forHeaderFooterViewReuseIdentifier: itemHeader.reuseIdentifier())
        }
    }
    
    public func reloadData() {
        tableView?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.cell(for: indexPath)
        
        let cell: BaseCell = CellFactory().cell(tableView: tableView, item: item!)
        cell.updateCell(forCellItem:item!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let item = self.cell(for: indexPath)
        return (item?.deleteAction != nil)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let item = self.cell(for: indexPath)
            item?.deleteAction?()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let item = self.cell(for: indexPath)
        item?.didDisplayAction?()
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying didEndDisplayingCell: UITableViewCell, forRowAt indexPath: IndexPath){
        let item = self.cell(for: indexPath)
        item?.didHideAction?()
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = self.cell(for: indexPath)
        item?.action?()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = self.cell(for: indexPath)

        if item?.forcedHeight == -1 {
            return UITableViewAutomaticDimension
        }else{
            return item!.forcedHeight
        }
    }
    
    func cell(for indexPath:IndexPath) -> Item? {
        return (cells.count > indexPath.row) ? cells[indexPath.row] : nil
    }
}
