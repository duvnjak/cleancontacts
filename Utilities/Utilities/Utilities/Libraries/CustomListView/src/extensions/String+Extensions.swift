// String+Extensions.swift
// Copyright (c) 2016 Nyx0uf
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


import Foundation


extension String
{
	// MARK: - Base64 encode
	func base64Encoded() -> String
	{
		if let utf8str = data(using: String.Encoding.utf8)
		{
			return utf8str.base64EncodedString(options: .lineLength64Characters)
		}
		return ""
	}

	// MARK: - Base64 decode
	func base64Decoded() -> String
	{
		if let base64data = Data(base64Encoded:self, options:[])
		{
			if let str = String(data:base64data, encoding:String.Encoding.utf8)
			{
				return str
			}
		}
		return ""
	}
    
    func isEmptyJsonString() -> Bool
    {
        return self == "{}"
    }
    
    func split(_ count: Int) -> [String] {
        return stride(from: 0, to: self.characters.count, by: count).map { i -> String in
            let startIndex = self.index(self.startIndex, offsetBy: i)
            let endIndex   = self.index(startIndex, offsetBy: count, limitedBy: self.endIndex) ?? self.endIndex
            return String(self[startIndex..<endIndex])
        }
    }
    
    func hexToDecimal() -> UInt8 {
        if let value = UInt8(self, radix: 16) {
            return value
        }
        return 0
    }
    
    func hexToInt() -> Int {
        if let value = Int(self, radix: 16) {
            return value
        }
        return 0
    }
    
    func hexToLong() -> Int {
        if let value = Int(self, radix: 32) {
            return value
        }
        return 0
    }
    
    func dropLast(_ n: Int = 1) -> String {
        return String(characters.dropLast(n))
    }
    
    var dropLast: String {
        return dropLast()
    }
    
    func swapFirstTwoAndLastTwo() -> String {
        return "\(self.suffix(2))\(self.prefix(2))"
    }
    
    func pad(toSize: Int) -> String {
        var padded = self
        for _ in 0..<(toSize - self.characters.count) {
            padded = "0" + padded
        }
        return padded
    }
    
    func toBinary(decimalValue:Int, toSize:Int = 16) -> String {
        let str = String(decimalValue, radix: 2)
        return str.pad(toSize: toSize)
    }
    
    func LsfToMsf() -> String {
        let array:[String] = self.split(4)
        let reversed = array.reversed()
        var finalArray = ""
        for singleHex in reversed{
            finalArray = "\(finalArray)\(singleHex.swapFirstTwoAndLastTwo())"
        }
        return finalArray
    }
}
