//
//  App.swift
//  Starter-iOS
//
//  Created by Arun Jangid on 4/13/17.
//  Copyright © 2017 Arun Jangid. All rights reserved.
//

import Foundation
import UIKit

struct App {
    static let screenWidth = UIScreen.main.bounds.width
    static let screenHeight = UIScreen.main.bounds.height
    
    struct AppColor {
        static let main = UIColor.red
        
        static let listBackgroundColor = UIColor(red: 0.94118, green: 0.81176, blue: 0.72157, alpha: 1.00)
        static let mainButtonColor = UIColor(red: 0.97647, green: 0.67451, blue: 0.71373, alpha: 1.00)
        static let tab1Color = UIColor(red: 0.94118, green: 0.81176, blue: 0.72157, alpha: 1.00)
        static let tab2Color = UIColor(red: 0.97647, green: 0.67451, blue: 0.71373, alpha: 1.00)
        static let tab3Color = UIColor(red: 0.49804, green: 0.49804, blue: 0.49804, alpha: 1.00)
        
        static let pagingStrokeUnselected = UIColor.clear
        static let pagingFillUnselected = UIColor(red: 0.89804, green: 0.89804, blue: 0.89804, alpha: 1.00)
        static let pagingStrokeSelected = UIColor.clear
        static let pagingFillSelected = UIColor(red: 0, green: 0, blue: 0, alpha: 1.00)

        //Not Used
        static let listBackgroundColor2 = UIColor(hex: 0xD6E3EC)
        
        struct FlowerKid {
            static let mainColor = UIColor(rgb: 0xf3897e)
            static let grayBGColor = UIColor(hexx: 0xefeff4)
            static let grayIcon = UIColor(hexx: 0x848c96)
            
            static let textBlack = UIColor(hexx: 0x262626)
            static let textGray = UIColor(hexx: 0x848c96)
            
            static let greenButton = UIColor(hexx: 0x54d76e)
            
            static let greenAlpha = UIColor(hexx: 0x21eaaa)
            static let greenOrange = UIColor(hexx: 0xffaf72)
            static let graphRed = UIColor(hexx: 0xed5959)
            static let blueButton = UIColor(rgb: 0x2faad9)
            
            static let orange = UIColor.orange
            static let red = UIColor.red
            
            static let normal = UIColor(rgb: 0x22eaaa)
            static let lowHigh = UIColor(rgb: 0xffb174)
            static let veryLowVeryHigh = UIColor(rgb: 0xee5a5a)
        }
        
        struct UserForm {
            static let titleTextColor = UIColor.black
            static let descriptionTextColor = UIColor.lightGray

            static let detailText = UIColor(red: 229/255, green: 65/255, blue: 88/255, alpha: 1.0)
            static let text = UIColor(red: 56/255, green: 56/255, blue: 56/255, alpha: 1.0)
            static let sectionBackground = UIColor.clear
            static let separator = UIColor(red: 182/255, green: 205/255, blue: 223/255, alpha: 1.0)
        }
    }
    
    struct Font {
        static func main(_ size: CGFloat) -> UIFont {
            return UIFont(name: "Helvetica", size: size) ?? UIFont.systemFont(ofSize: size)
        }
        static func subtitle(size: CGFloat) -> UIFont {
            return UIFont(name: "Helvetica", size: size) ?? UIFont.systemFont(ofSize: size)
        }
        static func description(size: CGFloat) -> UIFont {
            return UIFont(name: "Helvetica", size: size) ?? UIFont.systemFont(ofSize: size)
        }
    }
    
    
    static func setupDefaultAppearance() {
        // Icon color on Nav
        
        UIFont.overrideInitialize()
        self.setupDefaultNavBarAppearance()
        
        // Text color on Nav
//      UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: AppColor.main]

//      UILabel.appearance().substituteFontName = "KievitCompPro"

        // Text color (UIButton & UIAlertView & ...)
//        UIView.appearance().tintColor = Color.main
    }
    
    static func setupDefaultNavBarAppearance() {
        //UINavigationBar.appearance().tintColor = AppColor.main
        UINavigationBar.appearance().tintColor = UIColor.black
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black,
                                                            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)]
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black,
                                                             NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)], for: .normal)
    }

    
}
