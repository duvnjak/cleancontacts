//
//  AttributedStringExtension.swift
//  MobileSecurity
//
//  Created by Cristian Ganea on 10/03/2017.
//  Copyright © 2017 Avira. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    
    func setColorForText(textToFind: String, color: UIColor) {
        let range = self.mutableString.range(of: textToFind, options: .caseInsensitive)
        
        if range.location != NSNotFound {
            self.addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: range)
        }
    }
}
