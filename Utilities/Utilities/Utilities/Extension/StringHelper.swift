//
//  StringHelper.swift
//  Avira Cleaner
//
//  Created by Filip Duvnjak on 11/5/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation

extension String{
    func isValidEmail() -> Bool {
        
        //Check if nil
        if self.isEmpty {
            return false
        }
        //Remove whitespace
        let newInput = self.replacingOccurrences(of: " ", with: "")
        
        if newInput.isEmpty {
            return false
        }
        // Check regex
        let emailRegex = ".+@.+\\.[A-Za-z]{2}[A-Za-z]*"
        let emailTest = NSPredicate(format: "SELF MATCHES[c] %@", emailRegex)
        
        return emailTest.evaluate(with: newInput)
    }
    
    func isValidPassword() -> Bool {
        
        //Check if nil
        if self.isEmpty {
            return false
        }
        
        //Check min requirements
        if self.count < 5 {
            return false
        }
        
        return true
    }
    
    func generateRandomHash(length: Int, prefix: String) -> String {
        //Create alphabet
        let alphabet = self
        let s = NSMutableString(capacity: length)
        //Check for prefix
        if prefix != "" {
            s.append(prefix)
        }
        //Randomly add
        for _ in 0...length-1 {
            let r = arc4random() % UInt32(alphabet.count)
            let c = (alphabet as NSString).character(at: Int(r))
            s.appendFormat("%C", c)
        }
        
        return s as String
    }
}
