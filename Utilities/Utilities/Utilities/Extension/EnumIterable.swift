//
//  EnumIterable.swift
//  FlowerKid
//
//  Created by Davor Jankolija on 04/10/2017.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation

protocol EnumIterable: Sequence, RawRepresentable {
    static var any: Self { get }
    
    func toString() -> String
}

extension EnumIterable where RawValue == Int {
    
    func makeIterator() -> EnumIterator<Self> {
        return EnumIterator<Self>()
    }
    
    static var any: Self {
        return Self(rawValue: 0)!
    }
    
    func toString() -> String {
        return String(describing: self).wordsFromCamelCase()
    }
}

extension EnumIterable where Element == Self {
    static func create(from name: String?) -> Self? {
        guard let name = name else { return nil }
        return Self.any.first(where: { (element) -> Bool in
            return element.toString().capitalized == name
        })
    }
}

struct EnumIterator<T: RawRepresentable>: IteratorProtocol where T.RawValue == Int {
    typealias Element = T
    
    var index = 0
    
    mutating func next() -> Element? {
        let ethnicity = Element(rawValue: index)
        index += 1
        return ethnicity
    }
}

extension String {
    
    func wordsFromCamelCase() -> String {
        return unicodeScalars.reduce("") {
            if CharacterSet.uppercaseLetters.contains($1) {
                return ($0 + " " + String($1))
            } else {
                return $0 + String($1)
            }
        }
    }
    
}

