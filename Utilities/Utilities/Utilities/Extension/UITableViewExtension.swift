//
//  UITableViewExtension.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/4/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

extension UITableView{
    
    open func reloadSection(_ section: Int, with animation: UITableViewRowAnimation){
        self.reloadSections(NSIndexSet(index: section) as IndexSet, with: animation)
    }
    
    open func reloadAllSections(_ animation: UITableViewRowAnimation){
        let range = NSMakeRange(0, self.numberOfSections)
        let allSections = NSIndexSet(indexesIn: range)
        self.reloadSections(allSections as IndexSet, with: animation)
    }
    
    open func setAutomaticCellHeight(){
        self.estimatedRowHeight = 44.0
        self.rowHeight = UITableViewAutomaticDimension
    }
    
    open func setAutomaticSectionHeight(){
        self.sectionHeaderHeight = UITableViewAutomaticDimension
        self.estimatedSectionHeaderHeight = 44.0
    }
    
    func deselectSelectedRow() {
        if let row = self.indexPathForSelectedRow {
            self.deselectRow(at: row, animated: false)
        }
    }
    
    func removeExtraEmptyCells(){
        self.tableFooterView = UIView()
    }
}
