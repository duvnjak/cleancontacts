//
//  NumberExtensions.swift
//  Avira Cleaner
//
//  Created by Filip Duvnjak on 11/30/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
