//
//  UIViewControllerChildExtension.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/6/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func configureChildViewController(childController: UIViewController, onView: UIView?) {
        var holderView = self.view
        if let onView = onView {
            holderView = onView
        }
        addChildViewController(childController)
        holderView?.addSubview(childController.view)
        childController.view.tieConstraintsToMatchView(holderView: holderView!)
        childController.didMove(toParentViewController: self)
    }
    
    func constrainViewEqual(holderView: UIView, view: UIView) {
        
    }
}

extension UIView {
    func addSubviewWithFullscreenConstraints(subview:UIView) {
        self.addSubview(subview)
        subview.tieConstraintsToMatchView(holderView: self)
    }
    
    func tieConstraintsToMatchView(holderView:UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        //pin 100 points from the top of the super
        let pinTop = NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal,
                                        toItem: holderView, attribute: .top, multiplier: 1.0, constant: 0)
        let pinBottom = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal,
                                           toItem: holderView, attribute: .bottom, multiplier: 1.0, constant: 0)
        let pinLeft = NSLayoutConstraint(item: self, attribute: .left, relatedBy: .equal,
                                         toItem: holderView, attribute: .left, multiplier: 1.0, constant: 0)
        let pinRight = NSLayoutConstraint(item: self, attribute: .right, relatedBy: .equal,
                                          toItem: holderView, attribute: .right, multiplier: 1.0, constant: 0)
        
        holderView.addConstraints([pinTop, pinBottom, pinLeft, pinRight])
    }
    
}

