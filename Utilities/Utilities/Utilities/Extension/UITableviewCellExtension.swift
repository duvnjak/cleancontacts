//
//  UITableviewCellExtension.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/9/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell{
    func hideSeparator() {
        self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
    }
    
    func showDefaultSeparator() {
        self.separatorInset = UIEdgeInsetsMake(0, self.layoutMargins.left, 0, 0)
    }
    
    func showEndToEndSeparator() {
        self.separatorInset = .zero
    }
}
