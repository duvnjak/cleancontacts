//
//  UILocalNotificationExtension.swift
//  Avira Cleaner
//
//  Created by Filip Duvnjak on 11/15/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

extension UILocalNotification{
    static let userInfoIdentifierKey = "identifier"
    
    func setIdentifier(identifer:String) {
        var userInfo = [String:String]()
        userInfo[UILocalNotification.userInfoIdentifierKey] = identifer
        self.userInfo = userInfo
    }
    
    func identifier() -> String? {
        if let notificationIdentifier = self.userInfo![UILocalNotification.userInfoIdentifierKey] as? String {
            return notificationIdentifier;
        }
        return nil
    }
    
}
