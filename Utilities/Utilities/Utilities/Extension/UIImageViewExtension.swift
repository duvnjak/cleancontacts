//
//  UIImageViewExtension.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 5/8/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

extension UIBarButtonItem{
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

extension UIButton{
    func setImageColor(color: UIColor) {
        let templateImage = self.image(for: .normal)?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.setImage(templateImage, for: .normal)
        self.tintColor = color
    }
}

//let imageView = UIImageView(image: UIImage(named: "your_image_name"))
//imageView.setImageColor(color: UIColor.purple)

