//
//  ColorExtension.swift
//  MobileSecurity
//
//  Created by Cristian Ganea on 10/03/2017.
//  Copyright © 2017 Avira. All rights reserved.
//

import UIKit
    
@objc extension UIColor {
    func navbar_text() -> UIColor {
        return UIColor(hex: "7e93a7")
    }
    
    func drawer_text_top() -> UIColor {
        return UIColor(hex: "fefefe")
    }
    
    func drawer_menu_button() -> UIColor {
        return UIColor(hex: "7e93a7")
    }
    
    func drawer_text_bottom() -> UIColor {
        return UIColor(hex: "818d93")
    }
    
    func drawer_button() -> UIColor {
        return UIColor(hex: "23be04")
    }
    
    func drawer_button_border() -> UIColor {
        return UIColor(hex: "219709")
    }
    
    func welcome_background() -> UIColor {
        return UIColor(hex: "FCFFFF")
    }
    
    func welcome_signinTitle() -> UIColor {
        return UIColor(hex: "2a5082")
    }
    
    func welcome_signinDescription() -> UIColor {
        return UIColor(hex: "3a3b3d")
    }
    
    func welcome_button_text() -> UIColor {
        return UIColor(hex: "444547")
    }
    
    func welcome_button_border() -> UIColor {
        return UIColor(hex: "cfd8dc")
    }
    
    func protection_text_header() -> UIColor {
        return UIColor(hex: "7e93a7")
    }
    
    func protection_cell_text() -> UIColor {
        return UIColor(hex: "6c7e90")
    }
    
    func protection_cell_border() -> UIColor {
        return UIColor(hex: "cfd8dc")
    }
    
    func protection_cell_button() -> UIColor {
        return UIColor(hex: "23be04")
    }
    
    func protection_cell_button_border() -> UIColor {
        return UIColor(hex: "219709")
    }
    
    func settings_cell_separator() -> UIColor {
        return UIColor(hex: "cfd8dc")
    }
    
    func settings_icon() -> UIColor {
        return UIColor(hex: "6c7e90")
    }
    
    func settings_signout_button() -> UIColor{
        return UIColor(hex: "9b1212")
    }
    
    func identity_guard_cell_separator() -> UIColor {
        return UIColor(hex: "CFD8DC")
    }
    
    func dev_analyzer_dot_green() -> UIColor {
        return UIColor(hex: "4BB13C")
    }
    
    func dev_analyzer_dot_blue() -> UIColor {
        return UIColor(hex: "4AA2F8")
    }
    
    func dev_analyzer_dot_lightBrown() -> UIColor {
        return UIColor(hex: "EDBA3E")
    }
    
    func dev_analyzer_dot_red() -> UIColor {
        return UIColor(hex: "F9444F")
    }
    
    func webprot_ftu_cell_text() -> UIColor {
        return UIColor(hex: "7C8289")
    }
    
    func webprot_ftu_cell_separator() -> UIColor {
        return UIColor(hex: "d0d0d0")
    }
    
    func webprot_ftu_background() -> UIColor {
        return UIColor(hex: "f2f4f4")
    }
    
    func contacts_backup_description() -> UIColor {
        return UIColor(hex: "6c7e90")
    }
    
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
    
    
    class func aviraBlueButtonFill() -> UIColor {
        return self.aviraBackgroundDarkBlue()
    }
    
    class func aviraBackgroundDarkBlue() -> UIColor {
        return UIColor(hex: "233455")
    }
    
    class func aviraBackground() -> UIColor {
        return UIColor(hex: "FCFEFF")
    }
    
    class func aviraTextGray() -> UIColor {
        return UIColor(red255: 53, green255: 64, blue255: 82)
    }
    
    class func aviraTextLightGray() -> UIColor {
        return UIColor(red255: 158, green255: 164, blue255: 175)
    }
    
    class func aviraDarkButtonFill() -> UIColor {
        return UIColor(red255: 53, green255: 64, blue255: 82)
    }
    
    class func aviraGrayButtonFill() -> UIColor {
        return UIColor(red255: 158, green255: 164, blue255: 175)
    }
    
    class func aviraCellBorder() -> UIColor {
        return UIColor(red255: 230, green255: 234, blue255: 238)
    }
    
    class func aviraOrane() -> UIColor {
        return UIColor(red255: 255, green255: 163, blue255: 18)
    }
    
    class func aviraRed() -> UIColor {
        return UIColor(hex: "E01B22")
    }
    
    class func aviraTextBlue() -> UIColor {
        return UIColor(red255: 8, green255: 143, blue255: 254)
    }
    
    func lighter(by percentage:CGFloat=30.0) -> UIColor? {
        return self.adjust(by: abs(percentage) )
    }
    
    func darker(by percentage:CGFloat=30.0) -> UIColor? {
        return self.adjust(by: -1 * abs(percentage) )
    }
    
    func adjust(by percentage:CGFloat=30.0) -> UIColor? {
        var r:CGFloat=0, g:CGFloat=0, b:CGFloat=0, a:CGFloat=0;
        if(self.getRed(&r, green: &g, blue: &b, alpha: &a)){
            return UIColor(red: min(r + percentage/100, 1.0),
                           green: min(g + percentage/100, 1.0),
                           blue: min(b + percentage/100, 1.0),
                           alpha: a)
        }else{
            return nil
        }
    }
}
