//
//  Image+Extension.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/3/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    class func image(named imageName: String, scaledTo newSize: CGSize) -> UIImage {
        let image = UIImage(named:imageName)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image?.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        return UIGraphicsGetImageFromCurrentImageContext()!
    }
    
    class func image(named imageName: String, scaledAspectFitToWidth width: CGFloat) -> UIImage {
        let originalImage = UIImage(named: imageName)!
        let scaledHeight = Float(originalImage.size.height) / ( Float(originalImage.size.width) / Float(width))
        return UIImage.image(named: imageName, scaledTo: CGSize(width: width, height: CGFloat(scaledHeight)))
    }
}
