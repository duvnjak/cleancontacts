//
//  SyncHelper.swift
//  Avira Cleaner
//
//  Created by Filip Duvnjak on 1/26/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation

class SyncHelper {
    func synced(_ lock: Any, closure: () -> ()) {
        objc_sync_enter(lock)
        closure()
        objc_sync_exit(lock)
    }
}
