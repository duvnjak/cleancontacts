//
//  StringImageExtension.swift
//  Avira Cleaner
//
//  Created by Filip Duvnjak on 3/26/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var image: UIImage {
        return UIImage(named:self)!
    }
}
