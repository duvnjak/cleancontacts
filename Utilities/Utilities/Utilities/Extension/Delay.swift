//
//  Delay.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/2/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation

func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
        completion()
    }
}

