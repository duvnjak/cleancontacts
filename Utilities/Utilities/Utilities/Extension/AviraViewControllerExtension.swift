//
//  AviraViewControllerExtension.swift
//  Avira Cleaner
//
//  Created by Filip Duvnjak on 8/21/18.
//  Copyright © 2018 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    func setupAndHandleAviraNavigationBar() {
        self.setupAviraNavigationBar(openMenu: {
            self.openMenu()
        }) {
            self.aviraIconToggled()
        }
    }
    
    func setupAviraNavigationBar(openMenu:@escaping (()->()), aviraIconToggled:@escaping (()->())) {
        let leftMenuBtn = UIBarButtonItem(image: UIImage(named: "optimizer-nav-menu"), style: .plain, target: self, action: nil)
        leftMenuBtn.actionClosure = {
            openMenu()
        }
        leftMenuBtn.accessibilityLabel = "menu-bar-open-button"
        
        self.navigationItem.leftBarButtonItem = leftMenuBtn
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor().drawer_menu_button()
        leftMenuBtn.isAccessibilityElement = true
        leftMenuBtn.accessibilityIdentifier = "menuButton"
        
        let image = UIImage(named: "optimizer-app-icon-transparent")?.withRenderingMode(.alwaysOriginal)
        let rightBarBtn = UIBarButtonItem(image:image , style: .plain, target: self, action: nil)
        rightBarBtn.actionClosure = {
            aviraIconToggled()
        }
        rightBarBtn.isEnabled = false
        
        self.navigationItem.rightBarButtonItem = rightBarBtn
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.red
        
        self.navigationItem.title = "AppTitleNavBar".localized
        
        App.setupDefaultNavBarAppearance()
    }
    
    @objc func aviraIconToggled() {
        //TODO: antipattern on iOS - remove the button with no action! Management decision!
    }
    
    func openMenu() {
//        AviraEventMonitor.clickOpenSlidingMenu()
//        self.slideMenuController()?.openLeft()
    }
}
