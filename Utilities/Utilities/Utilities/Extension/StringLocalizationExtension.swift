//
//  StringLocalizationExtension.swift
//  Avira Cleaner
//
//  Created by Filip Duvnjak on 11/13/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment:"")
    }
    
//    "You have been using this app for %d day(s)".localized(with: 0)
//    func localized(with variable: CVarArg) -> String {
//        return String(format: self.localized, [variable])
//    }
    
    func localized(with variables: [CVarArg]) -> String {
        return String(format: self.localized, variables)
    }
}
