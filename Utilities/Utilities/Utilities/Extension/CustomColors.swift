//
//  CustomColors.swift
//  FlowerKid
//
//  Created by Filip Duvnjak on 10/2/17.
//  Copyright © 2017 Filip Duvnjak. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    static let customLightOrange = UIColor(red: 0.94118, green: 0.81176, blue: 0.72157, alpha: 1.00)
    
    public static func color1() -> UIColor{
        return UIColor(red: 0.96, green: 0.61, blue: 0.58, alpha: 1.00)
    }
    
    public static func color2() -> UIColor{
        return UIColor(red: 1.00, green: 0.61, blue: 0.16, alpha: 1.00)
    }
    
    public static func color3() -> UIColor{
        return UIColor(red: 0.15, green: 0.67, blue: 0.99, alpha: 1.00)
    }
    
    public static func color4() -> UIColor{
        return UIColor(red: 0.51, green: 0.72, blue: 0.25, alpha: 1.00)
    }
    
    public static func random() -> UIColor{
        let hue : CGFloat = CGFloat(arc4random() % 256) / 256 // use 256 to get full range from 0.0 to 1.0
        let saturation : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from white
        let brightness : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from black
        
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1)
    }
    
    convenience init(red255: CGFloat, green255: CGFloat, blue255: CGFloat, alpha: CGFloat = 1.0){
        self.init(
            red:   red255 / 255.0,
            green: green255  / 255.0,
            blue:  blue255  / 255.0,
            alpha: alpha
        )
    }
    
    func intRepresentation() -> UInt  {
        var colorAsUInt : UInt32 = 0
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            
            colorAsUInt += UInt32(red * 255.0) << 16 +
                UInt32(green * 255.0) << 8 +
                UInt32(blue * 255.0)
        }
        return UInt(colorAsUInt)
    }
    
    convenience init(hexx:Int, alpha:CGFloat = 1.0) {
        self.init(
            red:   CGFloat((hexx & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hexx & 0x00FF00) >> 8)  / 255.0,
            blue:  CGFloat((hexx & 0x0000FF) >> 0)  / 255.0,
            alpha: alpha
        )
    }
    
   

    
    convenience init(hex:Int, alpha:CGFloat = 1.0) {
        self.init(
            red:   CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8)  / 255.0,
            blue:  CGFloat((hex & 0x0000FF) >> 0)  / 255.0,
            alpha: alpha
        )
    }

}

