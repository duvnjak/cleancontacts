//
//  ApplicationManager.swift
//  Titan
//
//  Created by Nghia Tran on 10/14/16.
//  Copyright © 2016 fe. All rights reserved.
//

import UIKit
//import VersionTrackerSwift
//import SwiftyBeaver
//import Fabric
//import Crashlytics

class ApplicationManager {
    
    // MARK: - Variable
    static let sharedInstance = ApplicationManager()
    
    // Global Date formatter
    lazy var globalDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        dateFormatter.locale = NSLocale.init(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'zzz'Z'"
        return dateFormatter
    }()
    
    
    // MARK: Public
    
    /// SDK
    func initAllSDKs(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        
//        Fabric.with([Crashlytics.self])
//        VersionTracker.track()
//        (AVConfig.sharedConfig() as! AVConfig).setupForAviraOptimizer()
//        AviraEventMonitor.shared.setup()
//
//        FlowResources.setup(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    
    /// Common
    func initCommon(window: UIWindow?) {
        // Logger
        self.initLogger()
        
        // Global Appearance
        self.initGlobalAppearance()
    }
}

// MARK: - Private
extension ApplicationManager {
    
    // Logger
    fileprivate func initGlobalAppearance() {
        
    }
}

// MARK: Logger
extension ApplicationManager {
    fileprivate func initLogger() {
//        let console = ConsoleDestination()
        
//        let log = SwiftyBeaver.self
//        log.addDestination(console)
        
//        self.testLogger()
    }
    
//    func testLogger() {
//        let log = SwiftyBeaver.self
//
//        SwiftyBeaver.self.verbose("")
//        //Testing log levels
//        log.verbose("not so important")
//        log.debug("something to debug")
//        log.info("a nice information")   // prio 3, INFO in blue
//        log.warning("oh no, that won’t be good")  // prio 4, WARNING in yellow
//        log.error("ouch, an error did occur!")  // prio 5, ERROR in red
//    }
}

